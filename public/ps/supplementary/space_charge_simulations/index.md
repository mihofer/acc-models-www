<h1> PS lattice for space charge simulations </h1>

In general, PyORBIT space charge simulations using the PS lattice can be very time consuming. 
It is therefore advisable to include only as few elements as required in the lattice, while removing all the others using the MAD-X SEQEDIT command. On this page a MAD-X example is presented, which allows to remove all auxiliary elements from the ring apart from the low-energy quadrupoles, one 10 MHz cavity and the wire scanners.
Based on the needs of a given study, additional elements can be included in the lattice by commenting the respective line in the file *remove_elements.seq*. 

The MAD-X example furthermore includes the possibility to cycle the sequence and to create a PTC flat file for simulations in PyORBIT.

??? "MAD-X example script to remove elements- [direct download](ps_sc_lhc.madx)"
        /**********************************************************************************
        *
        * MAD-X input script for space charge simulations using the LHC proton flat bottom 
        * optics (with low-chromaticity). A large number of elements are removed from the 
        * sequence to decrease tracking time.
        *
        * 31/03/2020 - Alexander Huschauer
        ************************************************************************************/
         
        /******************************************************************
         * Energy and particle type definition
         ******************************************************************/

        BEAM, PARTICLE=PROTON, PC = 2.14;
        BRHO      := BEAM->PC * 3.3356;

        /******************************************************************
         * Call lattice files
         ******************************************************************/

        call, file="../../2021/ps_mu.seq";
        call, file="../../2021/ps_ss.seq";
        call, file="../../2021/scenarios/lhc_proton/1_flat_bottom/ps_fb_lhc.str";

        /******************************************************************
         * SEQEDIT to remove unwanted elements
         ******************************************************************/
         
        use, sequence = PS;
        twiss, file = 'initial_lattice.tfs';

        seqedit,sequence = PS;
                flatten;
        endedit;

        seqedit,sequence = PS;
                call, file = 'remove_elements.seq';
                remove, element=SELECTED;
        endedit;

        /******************************************************************
         * Cycle the sequence to a different initial location if desired
         ******************************************************************/

        /*
        START_LATTICE: MARKER;

        seqedit,sequence = PS;
                flatten;
                REPLACE, ELEMENT=PR.BWSH65, BY=START_LATTICE;
                cycle , start = START_LATTICE;
        endedit;
        */

        /******************************************************************
         * Create flat file
         ******************************************************************/

        use, sequence = PS;
        twiss, file = 'simplified_lattice.tfs';

        ptc_create_universe;
        ptc_create_layout,time=true, model=2, exact=true, method=6, nst=5;
        ptc_script, file="./print_flat_file.ptc"; 
        ptc_end;


??? "SEQEDIT script - [direct download](remove_elements.seq)"
        select, flag=seqedit, clear;
        select, flag=seqedit, pattern = '^PA\.C10\.[1-8]';      ! remove all but one 10 MHz cavities
        select, flag=seqedit, pattern = '^PA\.C10\.96';         ! remove all but one 10 MHz cavities
        select, flag=seqedit, class = URP;
        select, flag=seqedit, class = BCW_A;
        select, flag=seqedit, class = KFA__001;
        select, flag=seqedit, class = MONDAFWP;
        select, flag=seqedit, class = MXNBAFWP;
        select, flag=seqedit, class = MDBBBCWP;
        select, flag=seqedit, class = UPH__001;
        select, flag=seqedit, class = BTVPL001;
        select, flag=seqedit, class = BPUWA;
        select, flag=seqedit, class = SMH__003;
        select, flag=seqedit, class = MQSAAIAP;
        select, flag=seqedit, class = MARKER;
        select, flag=seqedit, class = MQNBCAWP;
        select, flag=seqedit, class = UDP;
        select, flag=seqedit, class = MONAAFWP;
        select, flag=seqedit, class = MXNCAAWP;
        select, flag=seqedit, class = BFA;
        select, flag=seqedit, class = BTVMA001;
        select, flag=seqedit, class = MM_AAIAP;
        select, flag=seqedit, class = KFA__002;
        select, flag=seqedit, class = KFA__003;
        select, flag=seqedit, class = MCHBAWWP;
        select, flag=seqedit, class = MQNBAFAP;
        select, flag=seqedit, class = KFA__004;
        select, flag=seqedit, class = KFA__005;
        select, flag=seqedit, class = TDI__001;
        select, flag=seqedit, class = TDIRB001;
        select, flag=seqedit, class = SEH__001;
        select, flag=seqedit, class = DHZ;
        select, flag=seqedit, class = MX_ABIAP;
        select, flag=seqedit, class = MSG;
        select, flag=seqedit, class = BLG;
        select, flag=seqedit, class = MQNBDAAP;
        select, flag=seqedit, class = USP;
        select, flag=seqedit, class = MX_AAIAP;
        select, flag=seqedit, class = SMH__001;
        select, flag=seqedit, class = AARFB;
        select, flag=seqedit, class = UFB;
        select, flag=seqedit, class = BPUNO;
        select, flag=seqedit, class = VVS;
        select, flag=seqedit, class = SMH__007;
        select, flag=seqedit, class = SMH__004;
        select, flag=seqedit, class = MCVAAWAP;
        select, flag=seqedit, class = BCT;
        select, flag=seqedit, class = BCTF;
        select, flag=seqedit, class = ACC200;
        select, flag=seqedit, class = BPMTS;
        select, flag=seqedit, class = ACC40;
        select, flag=seqedit, class = ACC20;
        select, flag=seqedit, class = BGIHA;
        select, flag=seqedit, class = BTVMF005;
        select, flag=seqedit, class = MQNCHAWP;
        select, flag=seqedit, class = URL;
        select, flag=seqedit, class = MDBCAWWP;
        select, flag=seqedit, class = MTV;
        select, flag=seqedit, class = ACWFA;
        select, flag=seqedit, class = TPS;
        select, flag=seqedit, class = MCHBBWWP;
        select, flag=seqedit, class = URS;
        select, flag=seqedit, class = MQNCAAWP;
        select, flag=seqedit, class = KFB;
        select, flag=seqedit, class = BSGW;
        select, flag=seqedit, class = ULF;
        select, flag=seqedit, class = ULG;


??? "PTC script to print flat file - [direct download](print_flat_file.ptc)"
        SELECTLAYOUT
        1
        LMAX
        0.5
        PRINT FLAT FILE
        flat_file.flt
        return

??? "MAD-X strength file - [direct download](../../2021/scenarios/lhc_proton/1_flat_bottom/ps_fb_lhc.str)"
        /**********************************************************************************
        *                             SBENDs and MULTIPOLES in MUs
        ***********************************************************************************/

        k1_f               =      0.05705266952 ;
        k1_d               =     -0.05711581728 ;
        k2_f               =                  0 ;
        k2_d               =                  0 ;
        mpk2               =     -0.01858039299 ;
        mpk2_j             =      0.03934772381 ;
        mpk3_f             =      0.05812457857 ;
        mpk3_d             =      -0.1320148496 ;

        /**********************************************************************************
        *                                    PFW and F8L
        ***********************************************************************************/

        pfwk1_f            =   -3.379844971e-05 ;
        pfwk1_d            =   -9.178530094e-05 ;
        pfwk2_f            =      0.01600309081 ;
        pfwk2_d            =     -0.01800393197 ;
        pfwk3_f            =      0.02652730096 ;
        pfwk3_d            =      0.04980959617 ;
        f8lk1              =                  0 ;

        /**********************************************************************************
        *                                    Injection dipoles
        ***********************************************************************************/

        kbsw26             =                  0 ;
        kbsw40             =                  0 ;
        kbsw41             =                  0 ;
        kbsw42             =                  0 ;
        kbsw43             =                  0 ;
        kbsw44             =                  0 ;

        /**********************************************************************************
        *                                    Extraction dipoles
        ***********************************************************************************/

        kbsw12             =                  0 ;
        kbsw14             =                  0 ;
        kbsw20             =                  0 ;
        kbsw22             =                  0 ;
        kbsw23             =                  0 ;
        kbsw57             =                  0 ;

        /**********************************************************************************
        *                                      Quadrupoles
        ***********************************************************************************/

        kf                 =     -0.01607206721 ;
        kd                 =      0.02827806196 ;
        kqse               =                  0 ;
        kqke16             =                  0 ;
        kqlb               =                  0 ;

        /**********************************************************************************
        *                                       Sextupoles
        ***********************************************************************************/

        kxno39             =                  0 ;
        kxno55             =                  0 ;
        kxno               =                  0 ;
        kxse               =                  0 ;

        /**********************************************************************************
        *                                       Octupoles
        ***********************************************************************************/

        kono39             =                  0 ;
        kono55             =                  0 ;
        kodn               =                  0 ;


??? "MAD-X sequence of straight section elements - [direct download](../../2021/ps_ss.seq)"
        

          /**********************************************************************************
          *
          * PS Ring version (draft) LS2 in MAD X SEQUENCE format
          * Generated the 20-APR-2020 18:10:34 from Layout
          *
          ***********************************************************************************/


        /************************************************************************************/
        /*                       TYPES DEFINITION                                           */
        /************************************************************************************/

        //---------------------- HKICKER        ---------------------------------------------
        BFA__001  : HKICKER     , L := .54;       ! Bending magnet. Fast bumper
        BLG       : HKICKER     , L := .2;        ! Dipole magnet, bumper horizontal, type 213
        KFA__001  : HKICKER     , L := .892;      ! Fast Kicker
        KFA__002  : HKICKER     , L := 1.05;      ! Fast Kicker
        KFA__003  : HKICKER     , L := 2.4458;    ! Fast Kicker
        KFA__004  : HKICKER     , L := .888;      ! Fast Kicker
        KFA__005  : HKICKER     , L := 1.05;      ! Fast Kicker
        MCHBAWWP  : HKICKER     , L := .23;       ! Corrector magnet, dipole horizontal, type 205
        MCHBBWWP  : HKICKER     , L := .27;       ! Corrector magnet, dipole horizontal, type 206
        MDBBCWAP  : HKICKER     , L := .56;       ! Dipole magnet, bumper, new 209 type
        MDBCAWWP  : HKICKER     , L := .3;        ! Dipole magnet, bumper vertical, type 210
        MSBSW001  : HKICKER     , L := .334;      ! Magnet, Bumper, eddy current injection
        MSMIC003  : HKICKER     , L := 0;         ! Septum Magnet Injection - type C
        SEH__001  : HKICKER     , L := .8;        ! Electrostatic septum (ejection).
        SMH__001  : HKICKER     , L := 2.28;      ! Septum Magnet Horizontal
        SMH__003  : HKICKER     , L := .83;       ! Septum Magnet Horizontal
        SMH__004  : HKICKER     , L := .568;      ! Septum Magnet Horizontal
        SMH__007  : HKICKER     , L := .79;       ! Septum Magnet Horizontal
        //---------------------- KICKER         ---------------------------------------------
        KFB       : KICKER      , L := .92;       ! kicker (Damper)
        //---------------------- MARKER         ---------------------------------------------
        OMK       : MARKER      , L := 0;         ! PS markers
        //---------------------- MONITOR        ---------------------------------------------
        BCT       : MONITOR     , L := 0;         ! Beam current transformer
        BCTF      : MONITOR     , L := 0;         ! Fast Beam Current Transformers
        BCW_A     : MONITOR     , L := 0;         ! Beam Wall Current monitor type A
        BGIHA     : MONITOR     , L := 0;         ! Beam Gas Ionisation Horizontal, type A
        BGIVA     : MONITOR     , L := 0;         ! Beam Gas Ionisation Vertical, type A
        BPMTS     : MONITOR     , L := 0;         ! 2 planes Stripline BPM with 4 long electrodes for PS Tune
        BPPPA     : MONITOR     , L := 0;         ! Pickup for PS phase measurement (RF)
        BPRPA     : MONITOR     , L := 0;         ! Pickup for PS radial measurement (RF)
        BPTF_001  : MONITOR     , L := 0;         ! Beam Position Monitor for RF Transverse Feedback, variant 001 (UDP)
        BPULG     : MONITOR     , L := 0;         ! Pickup for PS orbit measurement
        BPUNO     : MONITOR     , L := 0;         ! Beam Position, Normal Pick-up
        BPUPQ     : MONITOR     , L := 0;         ! Pickup for PS tune measurement
        BPUWA     : MONITOR     , L := 0;         ! Beam Position Pickup, Wideband, type A
        BSGW      : MONITOR     , L := 0;         ! SEM Grid with Wire
        BTVMA001  : MONITOR     , L := 0;         ! Beam observation TV, with Magnetic coupling, type A, variant 001
        BTVMF005  : MONITOR     , L := 0;         ! Beam TV Motorised Flip, variant 005
        BTVPL001  : MONITOR     , L := 0;         ! Beam observation TV Pneumatic Linear, variant 001
        BWSRB     : MONITOR     , L := 0;         ! Beam Wire Scanner, Rotational, type B
        MSG       : MONITOR     , L := 0;         ! SEM grid
        MTV       : MONITOR     , L := 0;         ! TV screen (scintillation monitor)
        URP       : MONITOR     , L := 0;         ! Pick-up  (p.u. electrode). Resistive position.
        URS       : MONITOR     , L := 0;         ! Pick-up  (p.u. electrode). Wide band, resistive.(wall current moniteur).
        USP       : MONITOR     , L := 0;         ! Pick-ups  (p.u. electrode). Special (low intensity)
        //---------------------- OCTUPOLE       ---------------------------------------------
        MONAAFWP  : OCTUPOLE    , L := .22;       ! Octupole magnet, type 802
        MONDAFWP  : OCTUPOLE    , L := .5;        ! Octupole magnet, type MTE
        //---------------------- QUADRUPOLE     ---------------------------------------------
        MQNAAIAP  : QUADRUPOLE  , L := .12;       ! Quadrupole magnet, type 401
        MQNABIAP  : QUADRUPOLE  , L := .12;       ! Quadrupole magnet, type 402
        MQNBAFAP  : QUADRUPOLE  , L := .19;       ! Quadrupole magnet, type 406
        MQNBCAWP  : QUADRUPOLE  , L := .23;       ! Quadrupole magnet, type 407
        MQNBDAAP  : QUADRUPOLE  , L := .19;       ! Quadrupole magnet, type 408
        MQNCAAWP  : QUADRUPOLE  , L := .25;       ! Quadrupole magnet, type 409
        MQNCHAWP  : QUADRUPOLE  , L := .3;        ! Quadrupole magnet, type 414
        MQSAAIAP  : QUADRUPOLE  , L := .12;       ! Quadrupole magnet, skew, type 404
        QNSD      : QUADRUPOLE  , L := .12;       ! Normal size Skew quadrupole type D inside multipole assembly MM_AAIAP (Q403)
        QNSF      : QUADRUPOLE  , L := .12;       ! Normal size Skew quadrupole type F inside multipole assembly MM_AAIAP (Q403)
        //---------------------- RFCAVITY       ---------------------------------------------
        ACC10     : RFCAVITY    , L := 2.45;      ! 10 MHz radio frequency cavity
        ACC20     : RFCAVITY    , L := 1.05;      ! 20 MHz radio frequency cavity
        ACC200    : RFCAVITY    , L := .12;       ! RF Cavity 200 MHz
        ACC40     : RFCAVITY    , L := 1.05;      ! 40 MHz radio frequency cavity
        ACC80     : RFCAVITY    , L := 1.05;      ! 80 MHz radio frequency cavity
        ACWFA     : RFCAVITY    , L := .88;       ! Accelerating Cavities - Wide-band - Finemet - Type A (for PSring)
        //---------------------- SEXTUPOLE      ---------------------------------------------
        MXNBAFWP  : SEXTUPOLE   , L := .2;        ! Sextupole Magnet, type 608
        MXNCAAWP  : SEXTUPOLE   , L := .66;       ! Sextupole Magnet, type 610
        MX_AAIAP  : SEXTUPOLE   , L := .15;       ! Sextupole Magnet, type 601
        MX_ABIAP  : SEXTUPOLE   , L := .29;       ! Sextupole Magnet, type 602
        //---------------------- VKICKER        ---------------------------------------------
        DVT__403  : VKICKER     , L := 0;         ! DVT Vertical Kicker inside MM_AAIAP multipole
        MCVAAWAP  : VKICKER     , L := .12;       ! Corrector magnet, dipole vertical, type 202


        /************************************************************************************/
        /*                       PS STRAIGHT SECTIONS                                       */
        /************************************************************************************/

        SS01: SEQUENCE, refer = centre, L = 3;
          PR.XSE01.A          : MXNBAFWP     , at = .42          , slot_id = 2369746;
          PR.XSE01.B          : MXNBAFWP     , at = .675         , slot_id = 2369747;
        ENDSEQUENCE;

        SS02: SEQUENCE, refer = centre, L = 1.6;
          PA.UFB02            : BPUNO        , at = .0934        , slot_id = 2253327;
          PA.CWB02            : ACWFA        , at = .775         , slot_id = 10338559;
          PR.DVT02            : DVT__403     , at = 1.546593     , slot_id = 8178826, assembly_id= 8178825;
        ENDSEQUENCE;

        SS03: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM03            : BPUNO        , at = .0934        , slot_id = 2253342;
          PR.BCW03.A          : BCW_A        , at = .51375       , slot_id = 36269910;
          PR.BCW03.B          : BCW_A        , at = 1.03625      , slot_id = 36269911;
          PR.QSK03            : QNSF         , at = 1.486593     , slot_id = 2253345, assembly_id= 5365801;
        ENDSEQUENCE;

        SS04: SEQUENCE, refer = centre, L = 1.6;
          PE.KFA04            : KFA__004     , at = .694         , slot_id = 2369736;
          PR.QSK04            : QNSD         , at = 1.486593     , slot_id = 2253361, assembly_id= 5365802;
          PR.DVT04            : DVT__403     , at = 1.546593     , slot_id = 2253360, assembly_id= 5365802;
        ENDSEQUENCE;

        SS05: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM05            : BPUNO        , at = .0934        , slot_id = 2253375;
          PE.QKE16.05         : MQNCHAWP     , at = .436         , slot_id = 2253376;
          PR.DHZOC05          : MDBCAWWP     , at = .929         , slot_id = 8178824;
          PR.QFN05            : MQNAAIAP     , at = 1.486343     , slot_id = 2253379;
        ENDSEQUENCE;

        SS06: SEQUENCE, refer = centre, L = 3;
          PA.C200.06.A        : ACC200       , at = .5455        , slot_id = 2253393;
          PA.C200.06.B        : ACC200       , at = .9195        , slot_id = 2253394;
          PA.C200.06.C        : ACC200       , at = 1.2935       , slot_id = 2253395;
          PA.C200.06.D        : ACC200       , at = 1.6675       , slot_id = 2253396;
          PA.C200.06.E        : ACC200       , at = 2.0415       , slot_id = 2253397;
          PA.C200.06.F        : ACC200       , at = 2.4155       , slot_id = 2253398;
          PR.QDW06            : MQNABIAP     , at = 2.886693     , slot_id = 2253399;
        ENDSEQUENCE;

        SS07: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM07            : BPULG        , at = .0934        , slot_id = 2253413;
          PR.XSE07            : MXNCAAWP     , at = .616         , slot_id = 2253414;
          PR.QTRTB07          : MQNCAAWP     , at = 1.115        , slot_id = 2253416;
          PR.QSK07            : MQSAAIAP     , at = 1.485793     , slot_id = 2253417;
        ENDSEQUENCE;

        SS08: SEQUENCE, refer = centre, L = 1.6;
          PR.C80.08           : ACC80        , at = .775         , slot_id = 2253431;
          PR.QSK08            : QNSD         , at = 1.486593     , slot_id = 2253432, assembly_id= 5365803;
          PR.DVT08            : DVT__403     , at = 1.546593     , slot_id = 2253433, assembly_id= 5365803;
        ENDSEQUENCE;

        SS09: SEQUENCE, refer = centre, L = 1.6;
          PE.BFA09.P          : BFA__001     , at = .5795        , slot_id = 2253447, assembly_id= 54813709;
          PR.QFN09            : MQNAAIAP     , at = 1.486343     , slot_id = 2253449;
        ENDSEQUENCE;

        SS10: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM10            : BPUNO        , at = .0934        , slot_id = 2253463;
          PR.XSK10            : MX_AAIAP     , at = .78          , slot_id = 10414023;
          PR.VVS10            : OMK          , at = 1.25         , slot_id = 13720377;
          PR.QDN10            : MQNAAIAP     , at = 1.486343     , slot_id = 2253464;
        ENDSEQUENCE;

        SS11: SEQUENCE, refer = centre, L = 3;
          PA.C10.11           : ACC10        , at = 1.475        , slot_id = 2253478;
        ENDSEQUENCE;

        SS12: SEQUENCE, refer = centre, L = 1.6;
          PR.UEP12            : BPULG        , at = .0934        , slot_id = 2253493;
          PR.RAL12            : OMK          , at = .3077        , slot_id = 8178823;
          PE.BSW12            : MCHBAWWP     , at = .69908       , slot_id = 2253494;
          PR.DVT12            : MCVAAWAP     , at = .95508       , slot_id = 2253495;
        ENDSEQUENCE;

        SS13: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM13            : BPUNO        , at = .0934        , slot_id = 2253509;
          PE.KFA13            : KFA__004     , at = .694         , slot_id = 2369737;
        ENDSEQUENCE;

        SS14: SEQUENCE, refer = centre, L = 1.6;
          PR.XSK14            : MX_ABIAP     , at = .6231        , slot_id = 10414025;
          PE.BSW14            : MCHBAWWP     , at = 1.0911       , slot_id = 2253524;
        ENDSEQUENCE;

        SS15: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM15            : BPULG        , at = .0895        , slot_id = 2253538;
          PE.TPS15            : OMK          , at = .7766        , slot_id = 6894625;
        ENDSEQUENCE;

        SS16: SEQUENCE, refer = centre, L = 3;
          PE.BTV16            : MTV          , at = .3083        , slot_id = 2253555, assembly_id= 2253556;
          PE.SMH16            : SMH__001     , at = 1.5067       , slot_id = 2253556;
        ENDSEQUENCE;

        SS17: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM17            : BPULG        , at = .445         , slot_id = 2253570;
          PR.QFW17            : MQNABIAP     , at = 1.486693     , slot_id = 2253572;
        ENDSEQUENCE;

        SS18: SEQUENCE, refer = centre, L = 1.6;
          PA.UPH18            : BPPPA        , at = .0934        , slot_id = 2253586;
          PR.DHZOC18          : MCHBAWWP     , at = .41731       , slot_id = 2253587;
          PR.QDW18            : MQNABIAP     , at = 1.486693     , slot_id = 2253588;
        ENDSEQUENCE;

        SS19: SEQUENCE, refer = centre, L = 1.6;
          PE.BSW23.19         : MCHBAWWP     , at = .63275       , slot_id = 2253605;
          PR.QTRDA19          : MQNBAFAP     , at = 1.033        , slot_id = 2253606;
          PR.QSK19            : MQSAAIAP     , at = 1.485793     , slot_id = 2253607;
        ENDSEQUENCE;

        SS20: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM20            : BPUNO        , at = .0934        , slot_id = 2253621;
          PE.BSW20            : MCHBAWWP     , at = .465306      , slot_id = 2253622;
          PR.VVS20            : OMK          , at = 1.25         , slot_id = 13720428;
          PR.QSK20            : QNSD         , at = 1.486593     , slot_id = 2253623, assembly_id= 5365804;
          PR.DVT20            : DVT__403     , at = 1.546593     , slot_id = 2253624, assembly_id= 5365804;
        ENDSEQUENCE;

        SS21: SEQUENCE, refer = centre, L = 3;
          PE.KFA21            : KFA__004     , at = 2.094        , slot_id = 2369738;
          PR.QFN21            : MQNAAIAP     , at = 2.886343     , slot_id = 2253643;
        ENDSEQUENCE;

        SS22: SEQUENCE, refer = centre, L = 1.6;
          PA.URL22            : BPRPA        , at = .0934        , slot_id = 2253657;
          PI.BSW26.22         : MCHBBWWP     , at = .46628       , slot_id = 2253658;
          PE.BSW22            : MCHBAWWP     , at = .75628       , slot_id = 2253659;
          PR.DVT22            : MCVAAWAP     , at = 1.09328      , slot_id = 2253660;
          PR.QDW22            : MQNABIAP     , at = 1.486693     , slot_id = 2253661;
        ENDSEQUENCE;

        SS23: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM23            : BPUNO        , at = .0934        , slot_id = 2253675;
          PE.SEH23            : SEH__001     , at = .775         , slot_id = 2253676;
          PR.QSK23            : MQSAAIAP     , at = 1.485793     , slot_id = 2253678;
        ENDSEQUENCE;

        SS24: SEQUENCE, refer = centre, L = 1.6;
          PR.DVT24            : MCVAAWAP     , at = .81468       , slot_id = 2253692;
          PR.QSK24            : MQSAAIAP     , at = 1.485793     , slot_id = 2253693;
        ENDSEQUENCE;

        SS25: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM25            : BPULG        , at = .0934        , slot_id = 2253707;
          PE.QKE16.25         : MQNCHAWP     , at = .5205        , slot_id = 2253708;
        ENDSEQUENCE;

        SS26: SEQUENCE, refer = centre, L = 3;
          PI.SMH26            : SMH__007     , at = 1.8          , slot_id = 2253724;
          PI.BTV26            : BTVPL001     , at = 2.2465       , slot_id = 2253725, assembly_id= 2253724;
        ENDSEQUENCE;

        SS27: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM27            : BPULG        , at = .0934        , slot_id = 2253740;
          PR.QTRDA27          : MQNBAFAP     , at = .47482       , slot_id = 2253741;
          PE.BSW23.27         : MCHBAWWP     , at = 1.105578     , slot_id = 2253744;
          PR.QFW27            : MQNABIAP     , at = 1.486693     , slot_id = 2253745;
        ENDSEQUENCE;

        SS28: SEQUENCE, refer = centre, L = 1.6;
          PI.KFA28            : KFA__005     , at = .775         , slot_id = 2253760;
          PR.QDW28            : MQNABIAP     , at = 1.486693     , slot_id = 2253761;
        ENDSEQUENCE;

        SS29: SEQUENCE, refer = centre, L = 1.6;
          PR.QSE29            : MQNCAAWP     , at = .626         , slot_id = 2253775;
          PR.QTRDB29          : MQNBAFAP     , at = 1.08125      , slot_id = 2253777;
          PR.QSK29            : MQSAAIAP     , at = 1.485793     , slot_id = 2253778;
        ENDSEQUENCE;

        SS30: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM30            : BPULG        , at = .0934        , slot_id = 2253792;
          PR.DVT30            : MCVAAWAP     , at = .5465        , slot_id = 2253793;
          PI.BSW26.30         : MCHBBWWP     , at = .9505        , slot_id = 2253794;
          PR.VVS30            : OMK          , at = 1.21         , slot_id = 13720429;
          PR.QSK30            : MQSAAIAP     , at = 1.485793     , slot_id = 2253795;
        ENDSEQUENCE;

        SS31: SEQUENCE, refer = centre, L = 3;
          PR.QFW31            : MQNABIAP     , at = 2.886693     , slot_id = 2253811;
        ENDSEQUENCE;

        SS32: SEQUENCE, refer = centre, L = 1.6;
          PR.QDW32            : MQNABIAP     , at = 1.486693     , slot_id = 2253826;
        ENDSEQUENCE;

        SS33: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM33            : BPULG        , at = .0934        , slot_id = 2253840;
          PI.QLB33            : MQNCHAWP     , at = .52          , slot_id = 47643341;
          PR.QSK33            : MQSAAIAP     , at = 1.485793     , slot_id = 2253843;
        ENDSEQUENCE;

        SS34: SEQUENCE, refer = centre, L = 1.6;
          PR.BCT34            : BCT          , at = .775         , slot_id = 2253858;
          PR.QSK34            : QNSD         , at = 1.486593     , slot_id = 2253859, assembly_id= 5365805;
          PR.DVT34            : DVT__403     , at = 1.546593     , slot_id = 2253860, assembly_id= 5365805;
        ENDSEQUENCE;

        SS35: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM35            : BPUNO        , at = .0934        , slot_id = 2253874;
          PR.QFN35            : MQNAAIAP     , at = 1.486343     , slot_id = 2253877;
        ENDSEQUENCE;

        SS36: SEQUENCE, refer = centre, L = 3;
          PA.URL36            : BPRPA        , at = .0934        , slot_id = 2253891;
          PA.C10.36           : ACC10        , at = 1.475        , slot_id = 2253892;
          PR.QDN36            : MQNAAIAP     , at = 2.886343     , slot_id = 2253893;
        ENDSEQUENCE;

        SS37: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM37            : BPUNO        , at = .0934        , slot_id = 2253907;
          PR.QTRDB37          : MQNBDAAP     , at = .44075       , slot_id = 2253908;
          PR.QSK37            : QNSF         , at = 1.486593     , slot_id = 2253912, assembly_id= 5365806;
        ENDSEQUENCE;

        SS38: SEQUENCE, refer = centre, L = 1.6;
          PA.UPH38            : BPPPA        , at = .0934        , slot_id = 2253926;
          PR.BCT38            : BCTF         , at = .775         , slot_id = 2253927;
          PR.QSK38            : QNSD         , at = 1.486593     , slot_id = 2253928, assembly_id= 5365807;
          PR.DVT38            : DVT__403     , at = 1.546593     , slot_id = 2253929, assembly_id= 5365807;
        ENDSEQUENCE;

        SS39: SEQUENCE, refer = centre, L = 1.6;
          PR.XNO39.A          : MXNBAFWP     , at = .398         , slot_id = 2369739;
          PR.ONO39            : MONDAFWP     , at = .7745        , slot_id = 2369740;
          PR.XNO39.B          : MXNBAFWP     , at = 1.151        , slot_id = 2369741;
          PR.QFN39            : MQNAAIAP     , at = 1.486343     , slot_id = 2253944;
        ENDSEQUENCE;

        SS40: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM40            : BPUNO        , at = .0934        , slot_id = 2253958;
          PR.ODN40            : MONAAFWP     , at = .4045        , slot_id = 2253960;
          PI.BSW40            : MDBBCWAP     , at = .90665       , slot_id = 52436790;
          PR.QDN40            : MQNAAIAP     , at = 1.486343     , slot_id = 2253961;
        ENDSEQUENCE;

        SS41: SEQUENCE, refer = centre, L = 3;
          PR.VVS41            : OMK          , at = .3           , slot_id = 52434935;
          PR.QTRTA41          : MQNBCAWP     , at = 1.4815       , slot_id = 2253976;
          PI.BSW41            : MDBBCWAP     , at = 2.04675      , slot_id = 52434955;
          PR.QSK41            : MQSAAIAP     , at = 2.885793     , slot_id = 2253977;
        ENDSEQUENCE;

        SS42: SEQUENCE, refer = centre, L = 1.6;
          PI.BTV42            : BTVMA001     , at = .01277       , slot_id = 53611786;
          PI.BSW42            : MSBSW001     , at = .48286       , slot_id = 54016199, assembly_id= 43074350;
          PI.SMH42            : MSMIC003     , at = .77762       , slot_id = 54016163, assembly_id= 43074350;
          PI.SMH42.ENDMARKER  : OMK          , at = 1.30085      , slot_id = 54016163;
          PI.BSF42            : MSG          , at = 1.47735      , slot_id = 2253993;
        ENDSEQUENCE;

        SS43: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM43            : BPULG        , at = .0934        , slot_id = 2254008;
          PI.BSW43            : MDBBCWAP     , at = .64335       , slot_id = 52667940;
          PR.QSK43            : MQSAAIAP     , at = 1.485793     , slot_id = 2254011;
        ENDSEQUENCE;

        SS44: SEQUENCE, refer = centre, L = 1.6;
          PI.BSW44            : MDBBCWAP     , at = .90765       , slot_id = 52667955;
          PR.QSK44            : QNSD         , at = 1.486593     , slot_id = 2254027, assembly_id= 5365808;
          PR.DVT44            : DVT__403     , at = 1.546593     , slot_id = 2254028, assembly_id= 5365808;
        ENDSEQUENCE;

        SS45: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM45            : BPUNO        , at = .0934        , slot_id = 2254042;
          PI.KFA45            : KFA__002     , at = .775         , slot_id = 2254043;
          PR.QFN45            : MQNAAIAP     , at = 1.486343     , slot_id = 2254045;
        ENDSEQUENCE;

        SS46: SEQUENCE, refer = centre, L = 3;
          PA.C10.46           : ACC10        , at = 1.475        , slot_id = 2254059;
          PR.QDN46            : MQNAAIAP     , at = 2.886343     , slot_id = 2254060;
        ENDSEQUENCE;

        SS47: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM47            : BPUNO        , at = .0934        , slot_id = 2254074;
          PR.TDI47            : OMK          , at = .6877        , slot_id = 52785837;
          PR.QSK47            : MQSAAIAP     , at = 1.485793     , slot_id = 2254077;
        ENDSEQUENCE;

        SS48: SEQUENCE, refer = centre, L = 1.6;
          PI.BSG48            : BSGW         , at = .0934        , slot_id = 2254091;
          PR.TDI48            : OMK          , at = .6877        , slot_id = 52785853;
          PR.QSK48            : MQSAAIAP     , at = 1.485793     , slot_id = 2254093;
        ENDSEQUENCE;

        SS49: SEQUENCE, refer = centre, L = 1.6;
          PI.QLB49            : MQNCHAWP     , at = .52          , slot_id = 47643342;
          PR.QTRTA49          : MQNCAAWP     , at = 1.0925       , slot_id = 2254108;
          PR.QFN49            : MQNAAIAP     , at = 1.486343     , slot_id = 2254109;
        ENDSEQUENCE;

        SS50: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM50            : BPUNO        , at = .0934        , slot_id = 2254123;
          PR.ODN50            : MONAAFWP     , at = .9725        , slot_id = 2254124;
          PR.VVS50            : OMK          , at = 1.3023       , slot_id = 13720431;
          PR.QDN50            : MQNAAIAP     , at = 1.486343     , slot_id = 2254125;
        ENDSEQUENCE;

        SS51: SEQUENCE, refer = centre, L = 3;
          PA.URL51            : BPRPA        , at = .0934        , slot_id = 2254139;
          PA.C10.51           : ACC10        , at = 1.475        , slot_id = 2254140;
        ENDSEQUENCE;

        SS52: SEQUENCE, refer = centre, L = 1.6;
          PI.BSG52            : BSGW         , at = .0934        , slot_id = 2254155;
          PR.ODN52.A          : MONAAFWP     , at = .5155        , slot_id = 10430397;
          PR.XSK52            : MX_AAIAP     , at = .75          , slot_id = 10414027;
          PR.ODN52.B          : MONAAFWP     , at = .9955        , slot_id = 2254156;
          PR.QSK52            : MQSAAIAP     , at = 1.485793     , slot_id = 2253994;
        ENDSEQUENCE;

        SS53: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM53            : BPUNO        , at = .0934        , slot_id = 2254170;
          PE.BSW57.53         : MCHBBWWP     , at = .8333        , slot_id = 2254171;
          PR.QSK53            : QNSF         , at = 1.486593     , slot_id = 2254173, assembly_id= 5365809;
        ENDSEQUENCE;

        SS54: SEQUENCE, refer = centre, L = 1.6;
          PI.BSG54            : BSGW         , at = .0934        , slot_id = 2254188;
          PR.BPM54            : BPUNO        , at = .357         , slot_id = 10414030;
          PR.BWSH54           : BWSRB        , at = .95343       , slot_id = 2254189;
          PR.QSK54            : QNSD         , at = 1.486593     , slot_id = 2254191, assembly_id= 5365810;
          PR.DVT54            : DVT__403     , at = 1.546593     , slot_id = 2254190, assembly_id= 5365810;
        ENDSEQUENCE;

        SS55: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM55            : BPUNO        , at = .0934        , slot_id = 2254205;
          PR.XNO55.A          : MXNBAFWP     , at = .3981        , slot_id = 2254206;
          PR.ONO55            : MONDAFWP     , at = .7746        , slot_id = 2369742;
          PR.XNO55.B          : MXNBAFWP     , at = 1.1512       , slot_id = 2254207;
          PR.QFN55            : MQNAAIAP     , at = 1.486343     , slot_id = 2254209;
        ENDSEQUENCE;

        SS56: SEQUENCE, refer = centre, L = 3;
          PA.C10.56           : ACC10        , at = 1.475        , slot_id = 2254223;
          PR.QDW56            : MQNABIAP     , at = 2.886693     , slot_id = 2254224;
        ENDSEQUENCE;

        SS57: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM57            : BPULG        , at = .0934        , slot_id = 2254238;
          PE.BTV57            : BTVMF005     , at = .3091        , slot_id = 2254242, assembly_id= 2254240;
          PE.SMH57            : SMH__003     , at = .805         , slot_id = 2254240;
          PR.QSK57            : MQSAAIAP     , at = 1.485793     , slot_id = 2254241;
        ENDSEQUENCE;

        SS58: SEQUENCE, refer = centre, L = 1.6;
          PR.XSK58            : MX_ABIAP     , at = .755         , slot_id = 10414032;
          PR.QSK58            : MQSAAIAP     , at = 1.485793     , slot_id = 2254256;
        ENDSEQUENCE;

        SS59: SEQUENCE, refer = centre, L = 1.6;
          PE.BSW57.59         : MCHBAWWP     , at = .4454        , slot_id = 2254271;
          PR.QFW59            : MQNABIAP     , at = 1.486693     , slot_id = 2254273;
        ENDSEQUENCE;

        SS60: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM60            : BPULG        , at = .0934        , slot_id = 2254287;
          PR.DHZOC60          : MCHBBWWP     , at = .434         , slot_id = 2254288;
          PR.XNO60            : MXNBAFWP     , at = .85          , slot_id = 10428678;
          PR.VVS60            : OMK          , at = 1.2094       , slot_id = 13720432;
          PR.QDW60            : MQNABIAP     , at = 1.486693     , slot_id = 2254289;
        ENDSEQUENCE;

        SS61: SEQUENCE, refer = centre, L = 3;
          PR.QTRDB61          : MQNBAFAP     , at = .43205       , slot_id = 2254304;
          PE.SMH61            : SMH__004     , at = .995         , slot_id = 2254307;
          PE.BSW57.61         : BLG          , at = 1.855        , slot_id = 2254305;
        ENDSEQUENCE;

        SS62: SEQUENCE, refer = centre, L = 1.6;
        ENDSEQUENCE;

        SS63: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM63            : BPULG        , at = .3685        , slot_id = 2254334;
        ENDSEQUENCE;

        SS64: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM64            : BPUNO        , at = .3587        , slot_id = 42548391;
          PR.BWSV64           : BWSRB        , at = .95343       , slot_id = 2254349;
          PR.DVT64            : DVT__403     , at = 1.546593     , slot_id = 2254351, assembly_id= 5365829;
        ENDSEQUENCE;

        SS65: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM65            : BPUNO        , at = .0934        , slot_id = 2254365;
          PR.BWSH65           : BWSRB        , at = .95343       , slot_id = 2369743;
        ENDSEQUENCE;

        SS66: SEQUENCE, refer = centre, L = 3;
          PA.C10.66           : ACC10        , at = 1.475        , slot_id = 2254380;
        ENDSEQUENCE;

        SS67: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM67            : BPUNO        , at = .0934        , slot_id = 2254394;
          PE.BSW57.67         : MCHBBWWP     , at = .4422        , slot_id = 2254395;
          PR.QFN67            : MQNAAIAP     , at = 1.486343     , slot_id = 2254398;
        ENDSEQUENCE;

        SS68: SEQUENCE, refer = centre, L = 1.6;
          PA.UPH68            : BPPPA        , at = .0934        , slot_id = 2254412;
          PR.BPM68            : BPUNO        , at = .357         , slot_id = 10414035;
          PR.BWSH68           : BWSRB        , at = .95343       , slot_id = 10414037;
          PR.QDN68            : MQNAAIAP     , at = 1.486343     , slot_id = 2254413;
        ENDSEQUENCE;

        SS69: SEQUENCE, refer = centre, L = 1.6;
          PR.BQS69            : BPUPQ        , at = .0934        , slot_id = 2254427;
          PR.QTRDB69          : MQNBDAAP     , at = 1.16075      , slot_id = 2254429;
          PR.QSK69            : QNSF         , at = 1.486593     , slot_id = 2254430, assembly_id= 5365811;
        ENDSEQUENCE;

        SS70: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM70            : BPUNO        , at = .0934        , slot_id = 2254445;
          PR.ODN70.A          : MONAAFWP     , at = .6305        , slot_id = 10430399;
          PR.ODN70.B          : MONAAFWP     , at = .978         , slot_id = 2254446;
          PR.VVS70            : OMK          , at = 1.344        , slot_id = 13720433;
          PR.QSK70            : QNSD         , at = 1.486593     , slot_id = 2254448, assembly_id= 5365812;
          PR.DVT70            : DVT__403     , at = 1.546593     , slot_id = 2254447, assembly_id= 5365812;
        ENDSEQUENCE;

        SS71: SEQUENCE, refer = centre, L = 3;
          PE.KFA71            : KFA__003     , at = 1.476        , slot_id = 2254462;
          PR.QFN71            : MQNAAIAP     , at = 2.886343     , slot_id = 2254464;
        ENDSEQUENCE;

        SS72: SEQUENCE, refer = centre, L = 1.6;
          PR.BQL72            : BPMTS        , at = .775         , slot_id = 6078900;
          PR.QDN72            : MQNAAIAP     , at = 1.486343     , slot_id = 2254479;
        ENDSEQUENCE;

        SS73: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM73            : BPUNO        , at = .0934        , slot_id = 2254493;
          PR.QTRTA73          : MQNBCAWP     , at = .4955        , slot_id = 2254494;
          PR.QSK73            : MQSAAIAP     , at = 1.485793     , slot_id = 2254497;
        ENDSEQUENCE;

        SS74: SEQUENCE, refer = centre, L = 1.6;
          PR.QSK74            : QNSD         , at = 1.486593     , slot_id = 2254511, assembly_id= 5365813;
          PR.DVT74            : DVT__403     , at = 1.546593     , slot_id = 2254512, assembly_id= 5365813;
        ENDSEQUENCE;

        SS75: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM75            : BPUNO        , at = .0934        , slot_id = 2254526;
        ENDSEQUENCE;

        SS76: SEQUENCE, refer = centre, L = 3;
          PA.URL76            : BPRPA        , at = .0934        , slot_id = 10410423;
          PA.C10.76           : ACC10        , at = 1.475        , slot_id = 2254542;
          PR.DVT76            : DVT__403     , at = 2.946593     , slot_id = 2254543, assembly_id= 5365830;
        ENDSEQUENCE;

        SS77: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM77            : BPUNO        , at = .0934        , slot_id = 2254558;
          PA.C40.77           : ACC40        , at = .771         , slot_id = 2254560;
          PR.QFN77            : MQNAAIAP     , at = 1.486343     , slot_id = 2254561;
        ENDSEQUENCE;

        SS78: SEQUENCE, refer = centre, L = 1.6;
          PA.C40.78           : ACC40        , at = .775         , slot_id = 2254575;
          PR.QDN78            : MQNAAIAP     , at = 1.486343     , slot_id = 2254576;
        ENDSEQUENCE;

        SS79: SEQUENCE, refer = centre, L = 1.6;
          PE.KFA79            : KFA__001     , at = .696         , slot_id = 2254591;
          PR.VVS79            : OMK          , at = 1.25         , slot_id = 13720434;
          PR.QSK79            : QNSF         , at = 1.486593     , slot_id = 2254593, assembly_id= 5365814;
        ENDSEQUENCE;

        SS80: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM80            : BPUNO        , at = .0934        , slot_id = 2254607;
          PA.C20.80           : ACC20        , at = .775         , slot_id = 2254608;
          PR.QSK80            : QNSD         , at = 1.486593     , slot_id = 2254609, assembly_id= 5365815;
          PR.DVT80            : DVT__403     , at = 1.546593     , slot_id = 2254610, assembly_id= 5365815;
        ENDSEQUENCE;

        SS81: SEQUENCE, refer = centre, L = 3;
          PA.C10.81           : ACC10        , at = 1.475        , slot_id = 2254624;
          PR.QFN81            : MQNAAIAP     , at = 2.886343     , slot_id = 2254626;
        ENDSEQUENCE;

        SS82: SEQUENCE, refer = centre, L = 1.6;
          PR.MDB82.STARTMARKER: OMK          , at = .595         , slot_id = 41442239;
          PR.BGI82            : BGIHA        , at = .775         , slot_id = 41479404, assembly_id= 41442239;
          PR.MDB82.ENDMARKER  : OMK          , at = .955         , slot_id = 41442239;
          PR.QDN82            : MQNAAIAP     , at = 1.486343     , slot_id = 2254641;
        ENDSEQUENCE;

        SS83: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM83            : BPULG        , at = .0934        , slot_id = 2254655;
          PR.USP83            : USP          , at = .775         , slot_id = 2254656;
          PR.QSK83            : QNSF         , at = 1.486593     , slot_id = 2254658, assembly_id= 5365816;
        ENDSEQUENCE;

        SS84: SEQUENCE, refer = centre, L = 1.6;
          PR.MDB84.STARTMARKER: OMK          , at = .595         , slot_id = 52734468;
          PR.BGI84            : BGIVA        , at = .775         , slot_id = 52734483, assembly_id= 52734468;
          PR.MDB84.ENDMARKER  : OMK          , at = .955         , slot_id = 52734468;
          PR.QSK84            : QNSD         , at = 1.486593     , slot_id = 2254675, assembly_id= 5365817;
        ENDSEQUENCE;

        SS85: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM85            : BPULG        , at = .0934        , slot_id = 2254689;
          PR.BWSV85           : BWSRB        , at = .95343       , slot_id = 2254690;
          PR.QFN85            : MQNAAIAP     , at = 1.486343     , slot_id = 2254692;
        ENDSEQUENCE;

        SS86: SEQUENCE, refer = centre, L = 3;
          PA.C10.86           : ACC10        , at = 1.475        , slot_id = 2254706;
          PR.QDN86            : MQNAAIAP     , at = 2.886343     , slot_id = 2254707;
        ENDSEQUENCE;

        SS87: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM87            : BPUNO        , at = .0934        , slot_id = 2254721;
          PR.QTRDA87          : MQNBDAAP     , at = .41675       , slot_id = 2254722;
          PR.QSE87            : MQNCAAWP     , at = .843         , slot_id = 2254724;
          PR.QSK87            : QNSF         , at = 1.486593     , slot_id = 2254726, assembly_id= 5365818;
        ENDSEQUENCE;

        SS88: SEQUENCE, refer = centre, L = 1.6;
          PA.UPH88            : BPPPA        , at = .0934        , slot_id = 2254740;
          PR.C80.88           : ACC80        , at = .775         , slot_id = 2254741;
          PR.QSK88            : QNSD         , at = 1.486593     , slot_id = 2254743, assembly_id= 5365819;
          PR.DVT88            : DVT__403     , at = 1.546593     , slot_id = 2254742, assembly_id= 5365819;
        ENDSEQUENCE;

        SS89: SEQUENCE, refer = centre, L = 1.6;
          PR.C80.89           : ACC80        , at = .775         , slot_id = 2254758;
          PR.QFN89            : MQNAAIAP     , at = 1.486343     , slot_id = 2254759;
        ENDSEQUENCE;

        SS90: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM90            : BPUNO        , at = .0934        , slot_id = 2254773;
          PR.BPMW90           : BPUWA        , at = .41          , slot_id = 42401369;
          PR.ODN90            : MONAAFWP     , at = .9892        , slot_id = 2254774;
          PR.VVS90            : OMK          , at = 1.25         , slot_id = 13720435;
          PR.QDN90            : MQNAAIAP     , at = 1.486343     , slot_id = 2254775;
        ENDSEQUENCE;

        SS91: SEQUENCE, refer = centre, L = 3;
          PA.C10.91           : ACC10        , at = 1.475        , slot_id = 2254789;
          PR.QSK91            : QNSF         , at = 2.886593     , slot_id = 2254792, assembly_id= 5365820;
        ENDSEQUENCE;

        SS92: SEQUENCE, refer = centre, L = 1.6;
          PR.C20.92           : ACC20        , at = .775         , slot_id = 2254806;
          PR.QSK92            : MQSAAIAP     , at = 1.485793     , slot_id = 2254807;
        ENDSEQUENCE;

        SS93: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM93            : BPULG        , at = .0934        , slot_id = 2254821;
          PR.USP93            : BPTF_001     , at = .775         , slot_id = 2254822;
          PR.QSK93            : MQSAAIAP     , at = 1.485793     , slot_id = 2254824;
        ENDSEQUENCE;

        SS94: SEQUENCE, refer = centre, L = 1.6;
          PR.XNO94            : MXNBAFWP     , at = .7706        , slot_id = 10428460;
          PR.UWB94            : BPUWA        , at = 1.12585      , slot_id = 2254839;
          PR.QSK94            : QNSD         , at = 1.486593     , slot_id = 2254840, assembly_id= 5365821;
          PR.DVT94            : DVT__403     , at = 1.546593     , slot_id = 2254841, assembly_id= 5365821;
        ENDSEQUENCE;

        SS95: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM95            : BPUNO        , at = .0934        , slot_id = 2254855;
          PR.UWB95            : URS          , at = .4624        , slot_id = 2254856;
          PR.QTRDA95          : MQNBDAAP     , at = 1.12215      , slot_id = 2254858;
          PR.QFN95            : MQNAAIAP     , at = 1.486343     , slot_id = 2254859;
        ENDSEQUENCE;

        SS96: SEQUENCE, refer = centre, L = 3;
          PA.URL96            : BPRPA        , at = .0934        , slot_id = 2254873;
          PA.C10.96           : ACC10        , at = 1.475        , slot_id = 2254874;
          PR.QDN96            : MQNAAIAP     , at = 2.886343     , slot_id = 2254875;
        ENDSEQUENCE;

        SS97: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM97            : BPUNO        , at = .0934        , slot_id = 2254889;
          PR.KFB97            : KFB          , at = .775         , slot_id = 2254890;
          PR.QSK97            : QNSF         , at = 1.486593     , slot_id = 2254893, assembly_id= 5365822;
        ENDSEQUENCE;

        SS98: SEQUENCE, refer = centre, L = 1.6;
          PR.UFB98            : BPUNO        , at = .0934        , slot_id = 2254907;
          PR.QSK98            : QNSD         , at = 1.486593     , slot_id = 2254910, assembly_id= 5365823;
          PR.DVT98            : DVT__403     , at = 1.546593     , slot_id = 2254909, assembly_id= 5365823;
        ENDSEQUENCE;

        SS99: SEQUENCE, refer = centre, L = 1.6;
          PR.QTRTB99.A        : MQNBCAWP     , at = .6532        , slot_id = 2254926;
          PR.QTRTB99.B        : MQNBCAWP     , at = 1.0782       , slot_id = 5706126;
          PR.QFN99            : MQNAAIAP     , at = 1.486343     , slot_id = 2254927;
        ENDSEQUENCE;

        SS00: SEQUENCE, refer = centre, L = 1.6;
          PR.BPM00            : BPUNO        , at = .0934        , slot_id = 2254941;
          PR.WCM00            : URP          , at = .5092        , slot_id = 2254942;
          PR.ODN00            : MONAAFWP     , at = .9508        , slot_id = 2254943;
          PR.VVS00            : OMK          , at = 1.251        , slot_id = 13720436;
          PR.QDN00            : MQNAAIAP     , at = 1.486343     , slot_id = 2254944;
        ENDSEQUENCE;


        /************************************************************************************/
        /*                       PS SECTORS                                                 */
        /************************************************************************************/

        SEC01 : SEQUENCE, refer = entry, L = 62.83185;
         SS01                               , at = 0;
         PR.BHT01                           , at = 3;
         SS02                               , at = 7.403185;
         PR.BHU02                           , at = 9.003185;
         SS03                               , at = 13.40637;
         PR.BHT03                           , at = 15.00637;
         SS04                               , at = 19.409555;
         PR.BHR04                           , at = 21.009555;
         SS05                               , at = 25.41274;
         PR.BHT05                           , at = 27.01274;
         SS06                               , at = 31.415925;
         PR.BHR06                           , at = 34.415925;
         SS07                               , at = 38.81911;
         PR.BHS07                           , at = 40.41911;
         SS08                               , at = 44.822295;
         PR.BHR08                           , at = 46.422295;
         SS09                               , at = 50.82548;
         PR.BHT09                           , at = 52.42548;
         SS10                               , at = 56.828665;
         PR.BHR10                           , at = 58.428665;
        ENDSEQUENCE;

        SEC02 : SEQUENCE, refer = entry, L = 62.83185;
         SS11                               , at = 0;
         PR.BHS11                           , at = 3;
         SS12                               , at = 7.403185;
         PR.BHR12                           , at = 9.003185;
         SS13                               , at = 13.40637;
         PR.BHS13                           , at = 15.00637;
         SS14                               , at = 19.409555;
         PR.BHU14                           , at = 21.009555;
         SS15                               , at = 25.41274;
         PR.BHT15                           , at = 27.01274;
         SS16                               , at = 31.415925;
         PR.BHU16                           , at = 34.415925;
         SS17                               , at = 38.81911;
         PR.BHT17                           , at = 40.41911;
         SS18                               , at = 44.822295;
         PR.BHU18                           , at = 46.422295;
         SS19                               , at = 50.82548;
         PR.BHS19                           , at = 52.42548;
         SS20                               , at = 56.828665;
         PR.BHR20                           , at = 58.428665;
        ENDSEQUENCE;

        SEC03 : SEQUENCE, refer = entry, L = 62.83185;
         SS21                               , at = 0;
         PR.BHT21                           , at = 3;
         SS22                               , at = 7.403185;
         PR.BHR22                           , at = 9.003185;
         SS23                               , at = 13.40637;
         PR.BHT23                           , at = 15.00637;
         SS24                               , at = 19.409555;
         PR.BHU24                           , at = 21.009555;
         SS25                               , at = 25.41274;
         PR.BHT25                           , at = 27.01274;
         SS26                               , at = 31.415925;
         PR.BHR26                           , at = 34.415925;
         SS27                               , at = 38.81911;
         PR.BHS27                           , at = 40.41911;
         SS28                               , at = 44.822295;
         PR.BHU28                           , at = 46.422295;
         SS29                               , at = 50.82548;
         PR.BHT29                           , at = 52.42548;
         SS30                               , at = 56.828665;
         PR.BHR30                           , at = 58.428665;
        ENDSEQUENCE;

        SEC04 : SEQUENCE, refer = entry, L = 62.83185;
         SS31                               , at = 0;
         PR.BHT31                           , at = 3;
         SS32                               , at = 7.403185;
         PR.BHR32                           , at = 9.003185;
         SS33                               , at = 13.40637;
         PR.BHS33                           , at = 15.00637;
         SS34                               , at = 19.409555;
         PR.BHR34                           , at = 21.009555;
         SS35                               , at = 25.41274;
         PR.BHT35                           , at = 27.01274;
         SS36                               , at = 31.415925;
         PR.BHR36                           , at = 34.415925;
         SS37                               , at = 38.81911;
         PR.BHT37                           , at = 40.41911;
         SS38                               , at = 44.822295;
         PR.BHR38                           , at = 46.422295;
         SS39                               , at = 50.82548;
         PR.BHT39                           , at = 52.42548;
         SS40                               , at = 56.828665;
         PR.BHU40                           , at = 58.428665;
        ENDSEQUENCE;

        SEC05 : SEQUENCE, refer = entry, L = 62.83185;
         SS41                               , at = 0;
         PR.BHT41                           , at = 3;
         SS42                               , at = 7.403185;
         PR.BHR42                           , at = 9.003185;
         SS43                               , at = 13.40637;
         PR.BHT43                           , at = 15.00637;
         SS44                               , at = 19.409555;
         PR.BHR44                           , at = 21.009555;
         SS45                               , at = 25.41274;
         PR.BHT45                           , at = 27.01274;
         SS46                               , at = 31.415925;
         PR.BHR46                           , at = 34.415925;
         SS47                               , at = 38.81911;
         PR.BHS47                           , at = 40.41911;
         SS48                               , at = 44.822295;
         PR.BHR48                           , at = 46.422295;
         SS49                               , at = 50.82548;
         PR.BHT49                           , at = 52.42548;
         SS50                               , at = 56.828665;
         PR.BHR50                           , at = 58.428665;
        ENDSEQUENCE;

        SEC06 : SEQUENCE, refer = entry, L = 62.83185;
         SS51                               , at = 0;
         PR.BHT51                           , at = 3;
         SS52                               , at = 7.403185;
         PR.BHR52                           , at = 9.003185;
         SS53                               , at = 13.40637;
         PR.BHS53                           , at = 15.00637;
         SS54                               , at = 19.409555;
         PR.BHR54                           , at = 21.009555;
         SS55                               , at = 25.41274;
         PR.BHT55                           , at = 27.01274;
         SS56                               , at = 31.415925;
         PR.BHU56                           , at = 34.415925;
         SS57                               , at = 38.81911;
         PR.BHT57                           , at = 40.41911;
         SS58                               , at = 44.822295;
         PR.BHU58                           , at = 46.422295;
         SS59                               , at = 50.82548;
         PR.BHT59                           , at = 52.42548;
         SS60                               , at = 56.828665;
         PR.BHU60                           , at = 58.428665;
        ENDSEQUENCE;

        SEC07 : SEQUENCE, refer = entry, L = 62.83185;
         SS61                               , at = 0;
         PR.BHT61                           , at = 3;
         SS62                               , at = 7.403185;
         PR.BHU62                           , at = 9.003185;
         SS63                               , at = 13.40637;
         PR.BHT63                           , at = 15.00637;
         SS64                               , at = 19.409555;
         PR.BHU64                           , at = 21.009555;
         SS65                               , at = 25.41274;
         PR.BHT65                           , at = 27.01274;
         SS66                               , at = 31.415925;
         PR.BHR66                           , at = 34.415925;
         SS67                               , at = 38.81911;
         PR.BHS67                           , at = 40.41911;
         SS68                               , at = 44.822295;
         PR.BHU68                           , at = 46.422295;
         SS69                               , at = 50.82548;
         PR.BHT69                           , at = 52.42548;
         SS70                               , at = 56.828665;
         PR.BHR70                           , at = 58.428665;
        ENDSEQUENCE;

        SEC08 : SEQUENCE, refer = entry, L = 62.83185;
         SS71                               , at = 0;
         PR.BHT71                           , at = 3;
         SS72                               , at = 7.403185;
         PR.BHR72                           , at = 9.003185;
         SS73                               , at = 13.40637;
         PR.BHS73                           , at = 15.00637;
         SS74                               , at = 19.409555;
         PR.BHU74                           , at = 21.009555;
         SS75                               , at = 25.41274;
         PR.BHT75                           , at = 27.01274;
         SS76                               , at = 31.415925;
         PR.BHR76                           , at = 34.415925;
         SS77                               , at = 38.81911;
         PR.BHT77                           , at = 40.41911;
         SS78                               , at = 44.822295;
         PR.BHR78                           , at = 46.422295;
         SS79                               , at = 50.82548;
         PR.BHS79                           , at = 52.42548;
         SS80                               , at = 56.828665;
         PR.BHR80                           , at = 58.428665;
        ENDSEQUENCE;

        SEC09 : SEQUENCE, refer = entry, L = 62.83185;
         SS81                               , at = 0;
         PR.BHT81                           , at = 3;
         SS82                               , at = 7.403185;
         PR.BHR82                           , at = 9.003185;
         SS83                               , at = 13.40637;
         PR.BHS83                           , at = 15.00637;
         SS84                               , at = 19.409555;
         PR.BHR84                           , at = 21.009555;
         SS85                               , at = 25.41274;
         PR.BHT85                           , at = 27.01274;
         SS86                               , at = 31.415925;
         PR.BHR86                           , at = 34.415925;
         SS87                               , at = 38.81911;
         PR.BHS87                           , at = 40.41911;
         SS88                               , at = 44.822295;
         PR.BHU88                           , at = 46.422295;
         SS89                               , at = 50.82548;
         PR.BHT89                           , at = 52.42548;
         SS90                               , at = 56.828665;
         PR.BHR90                           , at = 58.428665;
        ENDSEQUENCE;

        SEC10 : SEQUENCE, refer = entry, L = 62.83185;
         SS91                               , at = 0;
         PR.BHT91                           , at = 3;
         SS92                               , at = 7.403185;
         PR.BHR92                           , at = 9.003185;
         SS93                               , at = 13.40637;
         PR.BHS93                           , at = 15.00637;
         SS94                               , at = 19.409555;
         PR.BHR94                           , at = 21.009555;
         SS95                               , at = 25.41274;
         PR.BHT95                           , at = 27.01274;
         SS96                               , at = 31.415925;
         PR.BHR96                           , at = 34.415925;
         SS97                               , at = 38.81911;
         PR.BHT97                           , at = 40.41911;
         SS98                               , at = 44.822295;
         PR.BHR98                           , at = 46.422295;
         SS99                               , at = 50.82548;
         PR.BHS99                           , at = 52.42548;
         SS00                               , at = 56.828665;
         PR.BHR00                           , at = 58.428665;
        ENDSEQUENCE;


        /************************************************************************************/
        /*                       PS SEQUENCE                                                */
        /************************************************************************************/


        PS : SEQUENCE, refer = entry, L = 628.3185;
         SEC01                              , at = 0;
         SEC02                              , at = 62.83185;
         SEC03                              , at = 125.6637;
         SEC04                              , at = 188.49555;
         SEC05                              , at = 251.3274;
         SEC06                              , at = 314.15925;
         SEC07                              , at = 376.9911;
         SEC08                              , at = 439.82295;
         SEC09                              , at = 502.6548;
         SEC10                              , at = 565.48665;
        ENDSEQUENCE;

        /************************************************************************************/
        /*                       STRENGTH DEFINITIONS                                       */
        /************************************************************************************/

        PE.BSW12      , KICK := kBSW12;
        PE.BSW14      , KICK := kBSW14;
        PE.BSW20      , KICK := kBSW20;
        PE.BSW22      , KICK := kBSW22;
        PE.BSW23.19   , KICK := kBSW23;
        PE.BSW23.27   , KICK := kBSW23;
        PE.BSW57.53   , KICK :=-kBSW57*1.06492*PE.BSW57.59->L/PE.BSW57.53->L;
        PE.BSW57.59   , KICK :=+kBSW57; 
        PE.BSW57.61   , KICK :=-kBSW57*1.19887*PE.BSW57.59->L/PE.BSW57.61->L;  
        PE.BSW57.67   , KICK :=+kBSW57*1.06492*PE.BSW57.59->L/PE.BSW57.67->L;
        PE.KFA04      , KICK := kKFA04;
        PE.KFA13      , KICK := kKFA13;
        PE.KFA21      , KICK := kKFA21;
        PE.KFA71      , KICK := kKFA71;   
        PE.QKE16.05   , K1 :=-kQKE16;
        PE.QKE16.25   , K1 :=+kQKE16;
        PI.BSW26.22   , KICK := kBSW26;
        PI.BSW26.30   , KICK := kBSW26;
        PI.BSW40      , KICK := kBSW40;
        PI.BSW41      , KICK := kBSW41;
        PI.BSW42      , KICK := kBSW42;
        PI.BSW43      , KICK := kBSW43;
        PI.BSW44      , KICK := kBSW44;
        PI.QLB33      , K1 :=+kQLB;
        PI.QLB49      , K1 :=-kQLB;
        PI.KFA28      , KICK := kKFA28;
        PI.KFA45      , KICK := kKFA45;
        PR.DHZOC05    , KICK := kDHZOC05;
        PR.DHZOC18    , KICK := kDHZOC18;
        PR.DHZOC60    , KICK := kDHZOC60; 
        PR.DVT02      , KICK := kDVT02;  
        PR.DVT04      , KICK := kDVT04;
        PR.DVT08      , KICK := kDVT08;
        PR.DVT12      , KICK := kDVT12;
        PR.DVT20      , KICK := kDVT20;
        PR.DVT22      , KICK := kDVT22;
        PR.DVT24      , KICK := kDVT22;
        PR.DVT30      , KICK := kDVT22;
        PR.DVT34      , KICK := kDVT34;
        PR.DVT38      , KICK := kDVT38;
        PR.DVT44      , KICK := kDVT44;
        PR.DVT54      , KICK := kDVT54;
        PR.DVT64      , KICK := kDVT64;
        PR.DVT70      , KICK := kDVT70;
        PR.DVT74      , KICK := kDVT74;
        PR.DVT76      , KICK := kDVT76;
        PR.DVT80      , KICK := kDVT80;
        PR.DVT88      , KICK := kDVT88;
        PR.DVT94      , KICK := kDVT94;
        PR.DVT98      , KICK := kDVT98;
        PR.ODN00      , K3 := kODN;
        PR.ODN40      , K3 := kODN;
        PR.ODN50      , K3 := kODN;
        PR.ODN52.A    , K3 := kODN;
        PR.ODN52.B    , K3 := kODN;
        PR.ODN70.A    , K3 := kODN;
        PR.ODN70.B    , K3 := kODN;
        PR.ODN90      , K3 := kODN;
        PR.ONO39      , K3 := kONO39;
        PR.ONO55      , K3 := kONO55;
        PR.QDN00      , K1 := kQDN00;
        PR.QDN10      , K1 := kQDN10;
        PR.QDN36      , K1 := kQDN36;
        PR.QDN40      , K1 := kQDN40;
        PR.QDN46      , K1 := kQDN46;
        PR.QDN50      , K1 := kQDN50;
        PR.QDN68      , K1 := kQDN68;
        PR.QDN72      , K1 := kQDN72;
        PR.QDN78      , K1 := kQDN78;
        PR.QDN82      , K1 := kQDN82;
        PR.QDN86      , K1 := kQDN86;
        PR.QDN90      , K1 := kQDN90;
        PR.QDN96      , K1 := kQDN96;
        PR.QDW06      , K1 := kQDW06;
        PR.QDW18      , K1 := kQDW18;
        PR.QDW22      , K1 := kQDW22;
        PR.QDW28      , K1 := kQDW28;
        PR.QDW32      , K1 := kQDW32;
        PR.QDW56      , K1 := kQDW56;
        PR.QDW60      , K1 := kQDW60;
        PR.QFN05      , K1 := kQFN05;
        PR.QFN09      , K1 := kQFN09;
        PR.QFN21      , K1 := kQFN21;
        PR.QFN35      , K1 := kQFN35;
        PR.QFN39      , K1 := kQFN39;
        PR.QFN45      , K1 := kQFN45;
        PR.QFN49      , K1 := kQFN49;
        PR.QFN55      , K1 := kQFN55;
        PR.QFN67      , K1 := kQFN67;
        PR.QFN71      , K1 := kQFN71;
        PR.QFN77      , K1 := kQFN77;
        PR.QFN81      , K1 := kQFN81;
        PR.QFN85      , K1 := kQFN85;
        PR.QFN89      , K1 := kQFN89;
        PR.QFN95      , K1 := kQFN95;
        PR.QFN99      , K1 := kQFN99;
        PR.QFW17      , K1 := kQFW17;
        PR.QFW27      , K1 := kQFW27;
        PR.QFW31      , K1 := kQFW31;
        PR.QFW59      , K1 := kQFW59;
        PR.QSE29      , K1 := kQSE;
        PR.QSE87      , K1 := kQSE;  
        PR.QSK03      , K1S := kQSK03;
        PR.QSK04      , K1S := kQSK04;
        PR.QSK07      , K1S := kQSK07;
        PR.QSK08      , K1S := kQSK08;
        PR.QSK19      , K1S := kQSK19;
        PR.QSK20      , K1S := kQSK20;
        PR.QSK23      , K1S := kQSK23;
        PR.QSK24      , K1S := kQSK24;
        PR.QSK29      , K1S := kQSK29;
        PR.QSK30      , K1S := kQSK30;
        PR.QSK33      , K1S := kQSK33;
        PR.QSK34      , K1S := kQSK34;
        PR.QSK37      , K1S := kQSK37;
        PR.QSK38      , K1S := kQSK38;
        PR.QSK41      , K1S := kQSK41;
        PR.QSK43      , K1S := kQSK43;
        PR.QSK44      , K1S := kQSK44;
        PR.QSK47      , K1S := kQSK47;
        PR.QSK48      , K1S := kQSK48;
        PR.QSK52      , K1S := kQSK52;
        PR.QSK53      , K1S := kQSK53;
        PR.QSK54      , K1S := kQSK54;
        PR.QSK57      , K1S := kQSK57;
        PR.QSK58      , K1S := kQSK58;
        PR.QSK69      , K1S := kQSK69;
        PR.QSK70      , K1S := kQSK70;
        PR.QSK73      , K1S := kQSK73;
        PR.QSK74      , K1S := kQSK74;
        PR.QSK79      , K1S := kQSK79;
        PR.QSK80      , K1S := kQSK80;
        PR.QSK83      , K1S := kQSK83;
        PR.QSK84      , K1S := kQSK84;
        PR.QSK87      , K1S := kQSK87;
        PR.QSK88      , K1S := kQSK88;
        PR.QSK91      , K1S := kQSK91;
        PR.QSK92      , K1S := kQSK92;
        PR.QSK93      , K1S := kQSK93;
        PR.QSK94      , K1S := kQSK94;
        PR.QSK97      , K1S := kQSK97;
        PR.QSK98      , K1S := kQSK98;
        PR.QTRDA19    , K1 :=+kQTRDA;
        PR.QTRDA27    , K1 :=-kQTRDA;
        PR.QTRDA87    , K1 :=-kQTRDA*0.90934;
        PR.QTRDA95    , K1 :=+kQTRDA*0.90934;
        PR.QTRDB29    , K1 :=-kQTRDB;
        PR.QTRDB37    , K1 :=+kQTRDB*0.90934;
        PR.QTRDB61    , K1 :=+kQTRDB;
        PR.QTRDB69    , K1 :=-kQTRDB*0.90934;
        PR.QTRTA41    , K1 :=-kQTRTA*0.47968*PR.QTRTA49->L/PR.QTRTA41->L;
        PR.QTRTA49    , K1 :=+kQTRTA;
        PR.QTRTA73    , K1 :=-kQTRTA*0.47968*PR.QTRTA49->L/PR.QTRTA41->L;
        PR.QTRTB07    , K1 :=+kQTRTB;
        PR.QTRTB99.A  , K1 :=-kQTRTB*0.47968*PR.QTRTA49->L/PR.QTRTA41->L;
        PR.QTRTB99.B  , K1 :=-kQTRTB*0.47968*PR.QTRTA49->L/PR.QTRTA41->L;
        PR.XNO39.A    , K2 := kXNO39;
        PR.XNO39.B    , K2 := kXNO39;
        PR.XNO55.A    , K2 := kXNO55;
        PR.XNO55.B    , K2 := kXNO55; 
        PR.XNO60      , K2 := kXNO;
        PR.XNO94      , K2 := kXNO;
        PR.XSE01.A    , K2 := kXSE;
        PR.XSE01.B    , K2 := kXSE;
        PR.XSE07      , K2 := kXSE*2.81682*PR.XSE01.A->L/PR.XSE07->L;   
        PR.XSK10      , K2S := kXSK10;
        PR.XSK14      , K2S := kXSK14;
        PR.XSK52      , K2S := kXSK52;
        PR.XSK58      , K2S := kXSK58;

        KQDN00 := kd;
        KQDN10 := kd;
        KQDN36 := kd;
        KQDN40 := kd;
        KQDN46 := kd;
        KQDN50 := kd;
        KQDN68 := kd;
        KQDN72 := kd;
        KQDN78 := kd;
        KQDN82 := kd;
        KQDN86 := kd;
        KQDN90 := kd;
        KQDN96 := kd;
        KQDW06 := kd;
        KQDW18 := kd;
        KQDW22 := kd;
        KQDW28 := kd;
        KQDW32 := kd;
        KQDW56 := kd;
        KQDW60 := kd;
        KQFN05 := kf;
        KQFN09 := kf;
        KQFN21 := kf;
        KQFN35 := kf;
        KQFN39 := kf;
        KQFN45 := kf;
        KQFN49 := kf;
        KQFN55 := kf;
        KQFN67 := kf;
        KQFN71 := kf;
        KQFN77 := kf;
        KQFN81 := kf;
        KQFN85 := kf;
        KQFN89 := kf;
        KQFN95 := kf;
        KQFN99 := kf;   
        KQFW17 := kf;
        KQFW27 := kf;
        KQFW31 := kf;
        KQFW59 := kf;

        kQSK03 := kfs;
        kQSK04 := kds;
        kQSK07 := kfs;
        kQSK08 := kds;
        kQSK19 := kfs;
        kQSK20 := kds;
        kQSK23 := kfs;
        kQSK24 := kds;
        kQSK29 := kfs;
        kQSK30 := kds;
        kQSK33 := kfs;
        kQSK34 := kds;
        kQSK37 := kfs;
        kQSK38 := kds;
        kQSK41 := kfs;
        kQSK43 := kfs;
        kQSK44 := kds;
        kQSK47 := kfs;
        kQSK48 := kds;
        kQSK52 := kds;
        kQSK53 := kfs;
        kQSK54 := kds;
        kQSK57 := kfs;
        kQSK58 := kds;
        kQSK69 := kfs;
        kQSK70 := kds;
        kQSK73 := kfs;
        kQSK74 := kds;
        kQSK79 := kfs;
        kQSK80 := kds;
        kQSK83 := kfs;
        kQSK84 := kds;
        kQSK87 := kfs;
        kQSK88 := kds;
        kQSK91 := kfs;
        kQSK92 := kds;
        kQSK93 := kfs;
        kQSK94 := kds;
        kQSK97 := kfs;
        kQSK98 := kds;

        return;

??? "MAD-X sequence of main units - [direct download](../../2021/ps_mu.seq)"
        /**********************************************************************************
        *
        * Elements description and sequence file for each PS main unit (MU).
        *
        * 06/05/2020 - Alexander Huschauer
        ************************************************************************************/

        /************************************************************************************
        *
        *         DEFINITION OF FOCUSING AND DEFOCUSING HALF-UNITS OF THE MU               
        *
        *************************************************************************************/

        /************************************************************************************
        *        							 F HALF-UNITS     					            
        *************************************************************************************/

        LF   = +2.1260;				! iron length of F half-unit
        DLF := +0.0715925;          ! theoretical bending length correction; value has been rounded on drawing PS_LM___0013 (MU01 assembly) to 0.0716 m
        L_F := LF + DLF;			! total magnetic length

        ANGLE_F := 0.03135884818;  	! angle calculated according to EQ.? in NOTE
        K1_F := +0.05705266952;		! rematched for the bare machine
        K2_F :=  0.0;				! to be used in the presence of saturation effects in the MUs

        /************************************************************************************
        *        							 D HALF-UNITS     					            
        *************************************************************************************/

        LD   = +2.1340;          	! iron length of D half-unit
        DLD := +0.0715925;          ! theoretical bending length correction; value has been rounded on drawing PS_LM___0013 (MU01 assembly) to 0.0716 m
        L_D := LD + DLD;			! total magnetic length

        ANGLE_D := 0.03147300489;  	! angle calculated according to EQ.? in (CDS NOTE)
        K1_D := -0.05711581728;		! rematched for the bare machine
        K2_D :=  0.0;				! to be used in the presence of saturation effects in the MUs

        /************************************************************************************
        *        							 PFW and F8L    					            
        *************************************************************************************/

        PFWK1_F := 0.0; 			! representing the quadrupolar effect of the PFW
        PFWK2_F := 0.0;				! representing the sextupolar effect of the PFW
        PFWK3_F := 0.0;				! representing the octupolar effect of the PFW

        PFWK1_D := 0.0; 			! representing the quadrupolar effect of the PFW
        PFWK2_D := 0.0;				! representing the sextupolar effect of the PFW
        PFWK3_D := 0.0;				! representing the octupolar effect of the PFW

        F8LK1   := 0.0;				! representing the quadrupolar effect of the Figure-of-Eight-Loop

        /************************************************************************************
        *
        *                           MULTIPOLES INSIDE THE MAIN UNITS               
        * 
        *************************************************************************************/

        ! TODO: discuss whether inside or outside yoke description should still be retained
        ! ideally, information from the magnetic model to be incorporated here
        ! TODO: explain disappearance of the junction, being replaced by only a MULTIPOLE

        ! sextupole components matched for the bare machine
        MPK2    := -0.01858039299;
        MPK2_J  :=  0.03934772381;

        ! octupole components matched for the bare machine
        MPK3_F  :=  0.05812457857;
        MPK3_D  := -0.13201484960;

        MP.F: MULTIPOLE, knl := {0, 0, MPK2, MPK3_F + PFWK3_F, MPK4_F, MPK5_F};
        MP.J: MULTIPOLE, knl := {0, 0, MPK2_J, 0, MPK4_J, MPK5_J};
        MP.D: MULTIPOLE, knl := {0, 0, MPK2, MPK3_D + PFWK3_D, MPK4_D, MPK5_D};

        /************************************************************************************
        *
        *     DEFINITION OF HORIZONTAL ORBIT CORRECTORS REPRESENTING THE BACKLEG WINDINGS               
        *
        *************************************************************************************/

        ! In reality each DHZ corresponds to a cable around the yoke of two adjacent MUs. 
        ! For example, PR.DHZXX provides a correction along MU(XX-1) and MUXX.
        ! In this model, the effect of each PR.DHZXX is represented by kicks at the location 
        ! of the juntion of MU(XX-1) and MUXX.

        DHZ  : HKICKER  , L := 0;

        /************************************************************************************
        *
        *         						DEFINITION OF EACH MU               
        *
        *************************************************************************************/

        !R          ! D-F unit, yoke outside
        !S          ! F-D unit, yoke outside
        !T          ! F-D unit, yoke inside
        !U          ! D-F unit, yoke inside

        MU_L = L_F + L_D;		! Total magnetic length of one MU
        PR.BH.F: SBEND, L = L_F, ANGLE := ANGLE_F, K1 := K1_F + PFWK1_F - F8LK1, K2 := K2_F + PFWK2_F;
        PR.BH.D: SBEND, L = L_D, ANGLE := ANGLE_D, K1 := K1_D + PFWK1_D + F8LK1, K2 := K2_D + PFWK2_D;

        PR.BHT01: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP01.F:  MP.F,    AT = 0.0;
         PR.BHT01.F: PR.BH.F, AT = L_F/2;
         PR.MP01.J:  MP.J,    AT = L_F;
         PR.DHZ01.B: DHZ,     AT = L_F;
         PR.BHT01.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP01.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU02: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP02.D:  MP.D,    AT = 0.0;
         PR.BHU02.D: PR.BH.D, AT = L_D/2;
         PR.MP02.J:  MP.J,    AT = L_D;
         PR.DHZ03.A: DHZ,     AT = L_D;
         PR.BHU02.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP02.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT03: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP03.F:  MP.F,    AT = 0.0;
         PR.BHT03.F: PR.BH.F, AT = L_F/2;
         PR.MP03.J:  MP.J,    AT = L_F;
         PR.DHZ03.B: DHZ,     AT = L_F;
         PR.BHT03.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP03.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR04: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP04.D:  MP.D,    AT = 0.0;
         PR.BHR04.D: PR.BH.D, AT = L_D/2;
         PR.MP04.J:  MP.J,    AT = L_D;
         PR.DHZ05.A: DHZ,     AT = L_D;
         PR.BHR04.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP04.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT05: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP05.F:  MP.F,    AT = 0.0;
         PR.BHT05.F: PR.BH.F, AT = L_F/2;
         PR.MP05.J:  MP.J,    AT = L_F;
         PR.DHZ05.B: DHZ,     AT = L_F;
         PR.BHT05.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP05.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR06: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP06.D:  MP.D,    AT = 0.0;
         PR.BHR06.D: PR.BH.D, AT = L_D/2;
         PR.MP06.J:  MP.J,    AT = L_D;
         PR.DHZ07.A: DHZ,     AT = L_D;
         PR.BHR06.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP06.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS07: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP07.F:  MP.F,    AT = 0.0;
         PR.BHS07.F: PR.BH.F, AT = L_F/2;
         PR.MP07.J:  MP.J,    AT = L_F;
         PR.DHZ07.B: DHZ,     AT = L_F;
         PR.BHS07.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP07.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR08: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP08.D:  MP.D,    AT = 0.0;
         PR.BHR08.D: PR.BH.D, AT = L_D/2;
         PR.MP08.J:  MP.J,    AT = L_D;
         PR.DHZ09.A: DHZ,     AT = L_D;
         PR.BHR08.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP08.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT09: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP09.F:  MP.F,    AT = 0.0;
         PR.BHT09.F: PR.BH.F, AT = L_F/2;
         PR.MP09.J:  MP.J,    AT = L_F;
         PR.DHZ09.B: DHZ,     AT = L_F;
         PR.BHT09.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP09.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR10: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP10.D:  MP.D,    AT = 0.0;
         PR.BHR10.D: PR.BH.D, AT = L_D/2;
         PR.MP10.J:  MP.J,    AT = L_D;
         PR.DHZ11.A: DHZ,     AT = L_D;
         PR.BHR10.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP10.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS11: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP11.F:  MP.F,    AT = 0.0;
         PR.BHS11.F: PR.BH.F, AT = L_F/2;
         PR.MP11.J:  MP.J,    AT = L_F;
         PR.DHZ11.B: DHZ,     AT = L_F;
         PR.BHS11.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP11.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR12: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP12.D:  MP.D,    AT = 0.0;
         PR.BHR12.D: PR.BH.D, AT = L_D/2;
         PR.MP12.J:  MP.J,    AT = L_D;
         PR.DHZ13.A: DHZ,     AT = L_D;
         PR.BHR12.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP12.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS13: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP13.F:  MP.F,    AT = 0.0;
         PR.BHS13.F: PR.BH.F, AT = L_F/2;
         PR.MP13.J:  MP.J,    AT = L_F;
         PR.DHZ13.B: DHZ,     AT = L_F;
         PR.BHS13.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP13.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU14: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP14.D:  MP.D,    AT = 0.0;
         PR.BHU14.D: PR.BH.D, AT = L_D/2;
         PR.MP14.J:  MP.J,    AT = L_D;
         PR.DHZ15.A: DHZ,     AT = L_D;
         PR.BHU14.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP14.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT15: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP15.F:  MP.F,    AT = 0.0;
         PR.BHT15.F: PR.BH.F, AT = L_F/2;
         PR.MP15.J:  MP.J,    AT = L_F;
         PR.DHZ15.B: DHZ,     AT = L_F;
         PR.BHT15.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP15.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU16: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP16.D:  MP.D,    AT = 0.0;
         PR.BHU16.D: PR.BH.D, AT = L_D/2;
         PR.MP16.J:  MP.J,    AT = L_D;
         PR.DHZ17.A: DHZ,     AT = L_D;
         PR.BHU16.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP16.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT17: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP17.F:  MP.F,    AT = 0.0;
         PR.BHT17.F: PR.BH.F, AT = L_F/2;
         PR.MP17.J:  MP.J,    AT = L_F;
         PR.DHZ17.B: DHZ,     AT = L_F;
         PR.BHT17.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP17.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU18: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP18.D:  MP.D,    AT = 0.0;
         PR.BHU18.D: PR.BH.D, AT = L_D/2;
         PR.MP18.J:  MP.J,    AT = L_D;
         PR.DHZ19.A: DHZ,     AT = L_D;
         PR.BHU18.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP18.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS19: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP19.F:  MP.F,    AT = 0.0;
         PR.BHS19.F: PR.BH.F, AT = L_F/2;
         PR.MP19.J:  MP.J,    AT = L_F;
         PR.DHZ19.B: DHZ,     AT = L_F;
         PR.BHS19.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP19.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR20: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP20.D:  MP.D,    AT = 0.0;
         PR.BHR20.D: PR.BH.D, AT = L_D/2;
         PR.MP20.J:  MP.J,    AT = L_D;
         PR.DHZ21.A: DHZ,     AT = L_D;
         PR.BHR20.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP20.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT21: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP21.F:  MP.F,    AT = 0.0;
         PR.BHT21.F: PR.BH.F, AT = L_F/2;
         PR.MP21.J:  MP.J,    AT = L_F;
         PR.DHZ21.B: DHZ,     AT = L_F;
         PR.BHT21.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP21.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR22: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP22.D:  MP.D,    AT = 0.0;
         PR.BHR22.D: PR.BH.D, AT = L_D/2;
         PR.MP22.J:  MP.J,    AT = L_D;
         PR.DHZ23.A: DHZ,     AT = L_D;
         PR.BHR22.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP22.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT23: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP23.F:  MP.F,    AT = 0.0;
         PR.BHT23.F: PR.BH.F, AT = L_F/2;
         PR.MP23.J:  MP.J,    AT = L_F;
         PR.DHZ23.B: DHZ,     AT = L_F;
         PR.BHT23.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP23.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU24: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP24.D:  MP.D,    AT = 0.0;
         PR.BHU24.D: PR.BH.D, AT = L_D/2;
         PR.MP24.J:  MP.J,    AT = L_D;
         PR.DHZ25.A: DHZ,     AT = L_D;
         PR.BHU24.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP24.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT25: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP25.F:  MP.F,    AT = 0.0;
         PR.BHT25.F: PR.BH.F, AT = L_F/2;
         PR.MP25.J:  MP.J,    AT = L_F;
         PR.DHZ25.B: DHZ,     AT = L_F;
         PR.BHT25.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP25.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR26: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP26.D:  MP.D,    AT = 0.0;
         PR.BHR26.D: PR.BH.D, AT = L_D/2;
         PR.MP26.J:  MP.J,    AT = L_D;
         PR.DHZ27.A: DHZ,     AT = L_D;
         PR.BHR26.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP26.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS27: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP27.F:  MP.F,    AT = 0.0;
         PR.BHS27.F: PR.BH.F, AT = L_F/2;
         PR.MP27.J:  MP.J,    AT = L_F;
         PR.DHZ27.B: DHZ,     AT = L_F;
         PR.BHS27.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP27.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU28: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP28.D:  MP.D,    AT = 0.0;
         PR.BHU28.D: PR.BH.D, AT = L_D/2;
         PR.MP28.J:  MP.J,    AT = L_D;
         PR.DHZ29.A: DHZ,     AT = L_D;
         PR.BHU28.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP28.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT29: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP29.F:  MP.F,    AT = 0.0;
         PR.BHT29.F: PR.BH.F, AT = L_F/2;
         PR.MP29.J:  MP.J,    AT = L_F;
         PR.DHZ29.B: DHZ,     AT = L_F;
         PR.BHT29.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP29.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR30: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP30.D:  MP.D,    AT = 0.0;
         PR.BHR30.D: PR.BH.D, AT = L_D/2;
         PR.MP30.J:  MP.J,    AT = L_D;
         PR.DHZ31.A: DHZ,     AT = L_D;
         PR.BHR30.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP30.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT31: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP31.F:  MP.F,    AT = 0.0;
         PR.BHT31.F: PR.BH.F, AT = L_F/2;
         PR.MP31.J:  MP.J,    AT = L_F;
         PR.DHZ31.B: DHZ,     AT = L_F;
         PR.BHT31.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP31.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR32: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP32.D:  MP.D,    AT = 0.0;
         PR.BHR32.D: PR.BH.D, AT = L_D/2;
         PR.MP32.J:  MP.J,    AT = L_D;
         PR.DHZ33.A: DHZ,     AT = L_D;
         PR.BHR32.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP32.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS33: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP33.F:  MP.F,    AT = 0.0;
         PR.BHS33.F: PR.BH.F, AT = L_F/2;
         PR.MP33.J:  MP.J,    AT = L_F;
         PR.DHZ33.B: DHZ,     AT = L_F;
         PR.BHS33.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP33.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR34: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP34.D:  MP.D,    AT = 0.0;
         PR.BHR34.D: PR.BH.D, AT = L_D/2;
         PR.MP34.J:  MP.J,    AT = L_D;
         PR.DHZ35.A: DHZ,     AT = L_D;
         PR.BHR34.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP34.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT35: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP35.F:  MP.F,    AT = 0.0;
         PR.BHT35.F: PR.BH.F, AT = L_F/2;
         PR.MP35.J:  MP.J,    AT = L_F;
         PR.DHZ35.B: DHZ,     AT = L_F;
         PR.BHT35.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP35.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR36: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP36.D:  MP.D,    AT = 0.0;
         PR.BHR36.D: PR.BH.D, AT = L_D/2;
         PR.MP36.J:  MP.J,    AT = L_D;
         PR.DHZ37.A: DHZ,     AT = L_D;
         PR.BHR36.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP36.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT37: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP37.F:  MP.F,    AT = 0.0;
         PR.BHT37.F: PR.BH.F, AT = L_F/2;
         PR.MP37.J:  MP.J,    AT = L_F;
         PR.DHZ37.B: DHZ,     AT = L_F;
         PR.BHT37.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP37.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR38: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP38.D:  MP.D,    AT = 0.0;
         PR.BHR38.D: PR.BH.D, AT = L_D/2;
         PR.MP38.J:  MP.J,    AT = L_D;
         PR.DHZ39.A: DHZ,     AT = L_D;
         PR.BHR38.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP38.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT39: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP39.F:  MP.F,    AT = 0.0;
         PR.BHT39.F: PR.BH.F, AT = L_F/2;
         PR.MP39.J:  MP.J,    AT = L_F;
         PR.DHZ39.B: DHZ,     AT = L_F;
         PR.BHT39.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP39.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU40: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP40.D:  MP.D,    AT = 0.0;
         PR.BHU40.D: PR.BH.D, AT = L_D/2;
         PR.MP40.J:  MP.J,    AT = L_D;
         PR.DHZ41.A: DHZ,     AT = L_D;
         PR.BHU40.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP40.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT41: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP41.F:  MP.F,    AT = 0.0;
         PR.BHT41.F: PR.BH.F, AT = L_F/2;
         PR.MP41.J:  MP.J,    AT = L_F;
         PR.DHZ41.B: DHZ,     AT = L_F;
         PR.BHT41.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP41.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR42: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP42.D:  MP.D,    AT = 0.0;
         PR.BHR42.D: PR.BH.D, AT = L_D/2;
         PR.MP42.J:  MP.J,    AT = L_D;
         PR.DHZ43.A: DHZ,     AT = L_D;
         PR.BHR42.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP42.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT43: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP43.F:  MP.F,    AT = 0.0;
         PR.BHT43.F: PR.BH.F, AT = L_F/2;
         PR.MP43.J:  MP.J,    AT = L_F;
         PR.DHZ43.B: DHZ,     AT = L_F;
         PR.BHT43.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP43.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR44: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP44.D:  MP.D,    AT = 0.0;
         PR.BHR44.D: PR.BH.D, AT = L_D/2;
         PR.MP44.J:  MP.J,    AT = L_D;
         PR.DHZ45.A: DHZ,     AT = L_D;
         PR.BHR44.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP44.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT45: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP45.F:  MP.F,    AT = 0.0;
         PR.BHT45.F: PR.BH.F, AT = L_F/2;
         PR.MP45.J:  MP.J,    AT = L_F;
         PR.DHZ45.B: DHZ,     AT = L_F;
         PR.BHT45.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP45.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR46: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP46.D:  MP.D,    AT = 0.0;
         PR.BHR46.D: PR.BH.D, AT = L_D/2;
         PR.MP46.J:  MP.J,    AT = L_D;
         PR.DHZ47.A: DHZ,     AT = L_D;
         PR.BHR46.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP46.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS47: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP47.F:  MP.F,    AT = 0.0;
         PR.BHS47.F: PR.BH.F, AT = L_F/2;
         PR.MP47.J:  MP.J,    AT = L_F;
         PR.DHZ47.B: DHZ,     AT = L_F;
         PR.BHS47.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP47.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR48: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP48.D:  MP.D,    AT = 0.0;
         PR.BHR48.D: PR.BH.D, AT = L_D/2;
         PR.MP48.J:  MP.J,    AT = L_D;
         PR.DHZ49.A: DHZ,     AT = L_D;
         PR.BHR48.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP48.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT49: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP49.F:  MP.F,    AT = 0.0;
         PR.BHT49.F: PR.BH.F, AT = L_F/2;
         PR.MP49.J:  MP.J,    AT = L_F;
         PR.DHZ49.B: DHZ,     AT = L_F;
         PR.BHT49.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP49.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR50: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP50.D:  MP.D,    AT = 0.0;
         PR.BHR50.D: PR.BH.D, AT = L_D/2;
         PR.MP50.J:  MP.J,    AT = L_D;
         PR.DHZ51.A: DHZ,     AT = L_D;
         PR.BHR50.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP50.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT51: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP51.F:  MP.F,    AT = 0.0;
         PR.BHT51.F: PR.BH.F, AT = L_F/2;
         PR.MP51.J:  MP.J,    AT = L_F;
         PR.DHZ51.B: DHZ,     AT = L_F;
         PR.BHT51.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP51.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR52: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP52.D:  MP.D,    AT = 0.0;
         PR.BHR52.D: PR.BH.D, AT = L_D/2;
         PR.MP52.J:  MP.J,    AT = L_D;
         PR.DHZ53.A: DHZ,     AT = L_D;
         PR.BHR52.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP52.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS53: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP53.F:  MP.F,    AT = 0.0;
         PR.BHS53.F: PR.BH.F, AT = L_F/2;
         PR.MP53.J:  MP.J,    AT = L_F;
         PR.DHZ53.B: DHZ,     AT = L_F;
         PR.BHS53.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP53.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR54: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP54.D:  MP.D,    AT = 0.0;
         PR.BHR54.D: PR.BH.D, AT = L_D/2;
         PR.MP54.J:  MP.J,    AT = L_D;
         PR.DHZ55.A: DHZ,     AT = L_D;
         PR.BHR54.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP54.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT55: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP55.F:  MP.F,    AT = 0.0;
         PR.BHT55.F: PR.BH.F, AT = L_F/2;
         PR.MP55.J:  MP.J,    AT = L_F;
         PR.DHZ55.B: DHZ,     AT = L_F;
         PR.BHT55.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP55.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU56: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP56.D:  MP.D,    AT = 0.0;
         PR.BHU56.D: PR.BH.D, AT = L_D/2;
         PR.MP56.J:  MP.J,    AT = L_D;
         PR.DHZ57.A: DHZ,     AT = L_D;
         PR.BHU56.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP56.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT57: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP57.F:  MP.F,    AT = 0.0;
         PR.BHT57.F: PR.BH.F, AT = L_F/2;
         PR.MP57.J:  MP.J,    AT = L_F;
         PR.DHZ57.B: DHZ,     AT = L_F;
         PR.BHT57.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP57.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU58: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP58.D:  MP.D,    AT = 0.0;
         PR.BHU58.D: PR.BH.D, AT = L_D/2;
         PR.MP58.J:  MP.J,    AT = L_D;
         PR.DHZ59.A: DHZ,     AT = L_D;
         PR.BHU58.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP58.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT59: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP59.F:  MP.F,    AT = 0.0;
         PR.BHT59.F: PR.BH.F, AT = L_F/2;
         PR.MP59.J:  MP.J,    AT = L_F;
         PR.DHZ59.B: DHZ,     AT = L_F;
         PR.BHT59.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP59.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU60: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP60.D:  MP.D,    AT = 0.0;
         PR.BHU60.D: PR.BH.D, AT = L_D/2;
         PR.MP60.J:  MP.J,    AT = L_D;
         PR.DHZ61.A: DHZ,     AT = L_D;
         PR.BHU60.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP60.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT61: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP61.F:  MP.F,    AT = 0.0;
         PR.BHT61.F: PR.BH.F, AT = L_F/2;
         PR.MP61.J:  MP.J,    AT = L_F;
         PR.DHZ61.B: DHZ,     AT = L_F;
         PR.BHT61.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP61.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU62: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP62.D:  MP.D,    AT = 0.0;
         PR.BHU62.D: PR.BH.D, AT = L_D/2;
         PR.MP62.J:  MP.J,    AT = L_D;
         PR.DHZ63.A: DHZ,     AT = L_D;
         PR.BHU62.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP62.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT63: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP63.F:  MP.F,    AT = 0.0;
         PR.BHT63.F: PR.BH.F, AT = L_F/2;
         PR.MP63.J:  MP.J,    AT = L_F;
         PR.DHZ63.B: DHZ,     AT = L_F;
         PR.BHT63.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP63.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU64: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP64.D:  MP.D,    AT = 0.0;
         PR.BHU64.D: PR.BH.D, AT = L_D/2;
         PR.MP64.J:  MP.J,    AT = L_D;
         PR.DHZ65.A: DHZ,     AT = L_D;
         PR.BHU64.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP64.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT65: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP65.F:  MP.F,    AT = 0.0;
         PR.BHT65.F: PR.BH.F, AT = L_F/2;
         PR.MP65.J:  MP.J,    AT = L_F;
         PR.DHZ65.B: DHZ,     AT = L_F;
         PR.BHT65.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP65.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR66: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP66.D:  MP.D,    AT = 0.0;
         PR.BHR66.D: PR.BH.D, AT = L_D/2;
         PR.MP66.J:  MP.J,    AT = L_D;
         PR.DHZ67.A: DHZ,     AT = L_D;
         PR.BHR66.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP66.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS67: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP67.F:  MP.F,    AT = 0.0;
         PR.BHS67.F: PR.BH.F, AT = L_F/2;
         PR.MP67.J:  MP.J,    AT = L_F;
         PR.DHZ67.B: DHZ,     AT = L_F;
         PR.BHS67.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP67.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU68: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP68.D:  MP.D,    AT = 0.0;
         PR.BHU68.D: PR.BH.D, AT = L_D/2;
         PR.MP68.J:  MP.J,    AT = L_D;
         PR.DHZ69.A: DHZ,     AT = L_D;
         PR.BHU68.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP68.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT69: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP69.F:  MP.F,    AT = 0.0;
         PR.BHT69.F: PR.BH.F, AT = L_F/2;
         PR.MP69.J:  MP.J,    AT = L_F;
         PR.DHZ69.B: DHZ,     AT = L_F;
         PR.BHT69.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP69.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR70: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP70.D:  MP.D,    AT = 0.0;
         PR.BHR70.D: PR.BH.D, AT = L_D/2;
         PR.MP70.J:  MP.J,    AT = L_D;
         PR.DHZ71.A: DHZ,     AT = L_D;
         PR.BHR70.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP70.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT71: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP71.F:  MP.F,    AT = 0.0;
         PR.BHT71.F: PR.BH.F, AT = L_F/2;
         PR.MP71.J:  MP.J,    AT = L_F;
         PR.DHZ71.B: DHZ,     AT = L_F;
         PR.BHT71.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP71.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR72: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP72.D:  MP.D,    AT = 0.0;
         PR.BHR72.D: PR.BH.D, AT = L_D/2;
         PR.MP72.J:  MP.J,    AT = L_D;
         PR.DHZ73.A: DHZ,     AT = L_D;
         PR.BHR72.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP72.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS73: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP73.F:  MP.F,    AT = 0.0;
         PR.BHS73.F: PR.BH.F, AT = L_F/2;
         PR.MP73.J:  MP.J,    AT = L_F;
         PR.DHZ73.B: DHZ,     AT = L_F;
         PR.BHS73.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP73.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU74: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP74.D:  MP.D,    AT = 0.0;
         PR.BHU74.D: PR.BH.D, AT = L_D/2;
         PR.MP74.J:  MP.J,    AT = L_D;
         PR.DHZ75.A: DHZ,     AT = L_D;
         PR.BHU74.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP74.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT75: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP75.F:  MP.F,    AT = 0.0;
         PR.BHT75.F: PR.BH.F, AT = L_F/2;
         PR.MP75.J:  MP.J,    AT = L_F;
         PR.DHZ75.B: DHZ,     AT = L_F;
         PR.BHT75.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP75.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR76: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP76.D:  MP.D,    AT = 0.0;
         PR.BHR76.D: PR.BH.D, AT = L_D/2;
         PR.MP76.J:  MP.J,    AT = L_D;
         PR.DHZ77.A: DHZ,     AT = L_D;
         PR.BHR76.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP76.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT77: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP77.F:  MP.F,    AT = 0.0;
         PR.BHT77.F: PR.BH.F, AT = L_F/2;
         PR.MP77.J:  MP.J,    AT = L_F;
         PR.DHZ77.B: DHZ,     AT = L_F;
         PR.BHT77.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP77.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR78: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP78.D:  MP.D,    AT = 0.0;
         PR.BHR78.D: PR.BH.D, AT = L_D/2;
         PR.MP78.J:  MP.J,    AT = L_D;
         PR.DHZ79.A: DHZ,     AT = L_D;
         PR.BHR78.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP78.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS79: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP79.F:  MP.F,    AT = 0.0;
         PR.BHS79.F: PR.BH.F, AT = L_F/2;
         PR.MP79.J:  MP.J,    AT = L_F;
         PR.DHZ79.B: DHZ,     AT = L_F;
         PR.BHS79.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP79.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR80: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP80.D:  MP.D,    AT = 0.0;
         PR.BHR80.D: PR.BH.D, AT = L_D/2;
         PR.MP80.J:  MP.J,    AT = L_D;
         PR.DHZ81.A: DHZ,     AT = L_D;
         PR.BHR80.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP80.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT81: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP81.F:  MP.F,    AT = 0.0;
         PR.BHT81.F: PR.BH.F, AT = L_F/2;
         PR.MP81.J:  MP.J,    AT = L_F;
         PR.DHZ81.B: DHZ,     AT = L_F;
         PR.BHT81.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP81.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR82: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP82.D:  MP.D,    AT = 0.0;
         PR.BHR82.D: PR.BH.D, AT = L_D/2;
         PR.MP82.J:  MP.J,    AT = L_D;
         PR.DHZ83.A: DHZ,     AT = L_D;
         PR.BHR82.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP82.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS83: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP83.F:  MP.F,    AT = 0.0;
         PR.BHS83.F: PR.BH.F, AT = L_F/2;
         PR.MP83.J:  MP.J,    AT = L_F;
         PR.DHZ83.B: DHZ,     AT = L_F;
         PR.BHS83.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP83.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR84: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP84.D:  MP.D,    AT = 0.0;
         PR.BHR84.D: PR.BH.D, AT = L_D/2;
         PR.MP84.J:  MP.J,    AT = L_D;
         PR.DHZ85.A: DHZ,     AT = L_D;
         PR.BHR84.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP84.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT85: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP85.F:  MP.F,    AT = 0.0;
         PR.BHT85.F: PR.BH.F, AT = L_F/2;
         PR.MP85.J:  MP.J,    AT = L_F;
         PR.DHZ85.B: DHZ,     AT = L_F;
         PR.BHT85.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP85.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR86: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP86.D:  MP.D,    AT = 0.0;
         PR.BHR86.D: PR.BH.D, AT = L_D/2;
         PR.MP86.J:  MP.J,    AT = L_D;
         PR.DHZ87.A: DHZ,     AT = L_D;
         PR.BHR86.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP86.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS87: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP87.F:  MP.F,    AT = 0.0;
         PR.BHS87.F: PR.BH.F, AT = L_F/2;
         PR.MP87.J:  MP.J,    AT = L_F;
         PR.DHZ87.B: DHZ,     AT = L_F;
         PR.BHS87.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP87.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHU88: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP88.D:  MP.D,    AT = 0.0;
         PR.BHU88.D: PR.BH.D, AT = L_D/2;
         PR.MP88.J:  MP.J,    AT = L_D;
         PR.DHZ89.A: DHZ,     AT = L_D;
         PR.BHU88.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP88.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT89: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP89.F:  MP.F,    AT = 0.0;
         PR.BHT89.F: PR.BH.F, AT = L_F/2;
         PR.MP89.J:  MP.J,    AT = L_F;
         PR.DHZ89.B: DHZ,     AT = L_F;
         PR.BHT89.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP89.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR90: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP90.D:  MP.D,    AT = 0.0;
         PR.BHR90.D: PR.BH.D, AT = L_D/2;
         PR.MP90.J:  MP.J,    AT = L_D;
         PR.DHZ91.A: DHZ,     AT = L_D;
         PR.BHR90.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP90.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT91: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP91.F:  MP.F,    AT = 0.0;
         PR.BHT91.F: PR.BH.F, AT = L_F/2;
         PR.MP91.J:  MP.J,    AT = L_F;
         PR.DHZ91.B: DHZ,     AT = L_F;
         PR.BHT91.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP91.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR92: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP92.D:  MP.D,    AT = 0.0;
         PR.BHR92.D: PR.BH.D, AT = L_D/2;
         PR.MP92.J:  MP.J,    AT = L_D;
         PR.DHZ93.A: DHZ,     AT = L_D;
         PR.BHR92.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP92.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS93: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP93.F:  MP.F,    AT = 0.0;
         PR.BHS93.F: PR.BH.F, AT = L_F/2;
         PR.MP93.J:  MP.J,    AT = L_F;
         PR.DHZ93.B: DHZ,     AT = L_F;
         PR.BHS93.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP93.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR94: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP94.D:  MP.D,    AT = 0.0;
         PR.BHR94.D: PR.BH.D, AT = L_D/2;
         PR.MP94.J:  MP.J,    AT = L_D;
         PR.DHZ95.A: DHZ,     AT = L_D;
         PR.BHR94.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP94.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT95: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP95.F:  MP.F,    AT = 0.0;
         PR.BHT95.F: PR.BH.F, AT = L_F/2;
         PR.MP95.J:  MP.J,    AT = L_F;
         PR.DHZ95.B: DHZ,     AT = L_F;
         PR.BHT95.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP95.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR96: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP96.D:  MP.D,    AT = 0.0;
         PR.BHR96.D: PR.BH.D, AT = L_D/2;
         PR.MP96.J:  MP.J,    AT = L_D;
         PR.DHZ97.A: DHZ,     AT = L_D;
         PR.BHR96.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP96.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHT97: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP97.F:  MP.F,    AT = 0.0;
         PR.BHT97.F: PR.BH.F, AT = L_F/2;
         PR.MP97.J:  MP.J,    AT = L_F;
         PR.DHZ97.B: DHZ,     AT = L_F;
         PR.BHT97.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP97.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR98: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP98.D:  MP.D,    AT = 0.0;
         PR.BHR98.D: PR.BH.D, AT = L_D/2;
         PR.MP98.J:  MP.J,    AT = L_D;
         PR.DHZ99.A: DHZ,     AT = L_D;
         PR.BHR98.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP98.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHS99: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP99.F:  MP.F,    AT = 0.0;
         PR.BHS99.F: PR.BH.F, AT = L_F/2;
         PR.MP99.J:  MP.J,    AT = L_F;
         PR.DHZ99.B: DHZ,     AT = L_F;
         PR.BHS99.D: PR.BH.D, AT = L_F + L_D/2;
         PR.MP99.D:  MP.D,    AT = MU_L;
        ENDSEQUENCE;

        PR.BHR00: SEQUENCE, refer = CENTRE,  L = MU_L;
         PR.MP00.D:  MP.D,    AT = 0.0;
         PR.BHR00.D: PR.BH.D, AT = L_D/2;
         PR.MP00.J:  MP.J,    AT = L_D;
         PR.DHZ01.A: DHZ,     AT = L_D;
         PR.BHR00.F: PR.BH.F, AT = L_D + L_F/2;
         PR.MP00.F:  MP.F,    AT = MU_L;
        ENDSEQUENCE;

        /************************************************************************************/
        /*                       STRENGTH DEFINITIONS                                       */
        /************************************************************************************/

        PR.DHZ01.A, KICK := kDHZ01;
        PR.DHZ01.B, KICK := kDHZ01;
        PR.DHZ03.A, KICK := kDHZ03;
        PR.DHZ03.B, KICK := kDHZ03;
        PR.DHZ05.A, KICK := kDHZ05;
        PR.DHZ05.B, KICK := kDHZ05;
        PR.DHZ07.A, KICK := kDHZ07;
        PR.DHZ07.B, KICK := kDHZ07;
        PR.DHZ09.A, KICK := kDHZ09;
        PR.DHZ09.B, KICK := kDHZ09;
        PR.DHZ11.A, KICK := kDHZ11;
        PR.DHZ11.B, KICK := kDHZ11;
        PR.DHZ13.A, KICK := kDHZ13;
        PR.DHZ13.B, KICK := kDHZ13;
        PR.DHZ15.A, KICK := kDHZ15;
        PR.DHZ15.B, KICK := kDHZ15;
        PR.DHZ17.A, KICK := kDHZ17;
        PR.DHZ17.B, KICK := kDHZ17;
        PR.DHZ19.A, KICK := kDHZ19;
        PR.DHZ19.B, KICK := kDHZ19;
        PR.DHZ21.A, KICK := kDHZ21;
        PR.DHZ21.B, KICK := kDHZ21;
        PR.DHZ23.A, KICK := kDHZ23;
        PR.DHZ23.B, KICK := kDHZ23;
        PR.DHZ25.A, KICK := kDHZ25;
        PR.DHZ25.B, KICK := kDHZ25;
        PR.DHZ27.A, KICK := kDHZ27;
        PR.DHZ27.B, KICK := kDHZ27;
        PR.DHZ29.A, KICK := kDHZ29;
        PR.DHZ29.B, KICK := kDHZ29;
        PR.DHZ31.A, KICK := kDHZ31;
        PR.DHZ31.B, KICK := kDHZ31;
        PR.DHZ33.A, KICK := kDHZ33;
        PR.DHZ33.B, KICK := kDHZ33;
        PR.DHZ35.A, KICK := kDHZ35;
        PR.DHZ35.B, KICK := kDHZ35;
        PR.DHZ37.A, KICK := kDHZ37;
        PR.DHZ37.B, KICK := kDHZ37;
        PR.DHZ39.A, KICK := kDHZ39;
        PR.DHZ39.B, KICK := kDHZ39;
        PR.DHZ41.A, KICK := kDHZ41;
        PR.DHZ41.B, KICK := kDHZ41;
        PR.DHZ43.A, KICK := kDHZ43;
        PR.DHZ43.B, KICK := kDHZ43;
        PR.DHZ45.A, KICK := kDHZ45;
        PR.DHZ45.B, KICK := kDHZ45;
        PR.DHZ47.A, KICK := kDHZ47;
        PR.DHZ47.B, KICK := kDHZ47;
        PR.DHZ49.A, KICK := kDHZ49;
        PR.DHZ49.B, KICK := kDHZ49;
        PR.DHZ51.A, KICK := kDHZ51;
        PR.DHZ51.B, KICK := kDHZ51;
        PR.DHZ53.A, KICK := kDHZ53;
        PR.DHZ53.B, KICK := kDHZ53;
        PR.DHZ55.A, KICK := kDHZ55;
        PR.DHZ55.B, KICK := kDHZ55;
        PR.DHZ57.A, KICK := kDHZ57;
        PR.DHZ57.B, KICK := kDHZ57;
        PR.DHZ59.A, KICK := kDHZ59;
        PR.DHZ59.B, KICK := kDHZ59;
        PR.DHZ61.A, KICK := kDHZ61;
        PR.DHZ61.B, KICK := kDHZ61;
        PR.DHZ63.A, KICK := kDHZ63;
        PR.DHZ63.B, KICK := kDHZ63;
        PR.DHZ65.A, KICK := kDHZ65;
        PR.DHZ65.B, KICK := kDHZ65;
        PR.DHZ67.A, KICK := kDHZ67;
        PR.DHZ67.B, KICK := kDHZ67;
        PR.DHZ69.A, KICK := kDHZ69;
        PR.DHZ69.B, KICK := kDHZ69;
        PR.DHZ71.A, KICK := kDHZ71;
        PR.DHZ71.B, KICK := kDHZ71;
        PR.DHZ73.A, KICK := kDHZ73;
        PR.DHZ73.B, KICK := kDHZ73;
        PR.DHZ75.A, KICK := kDHZ75;
        PR.DHZ75.B, KICK := kDHZ75;
        PR.DHZ77.A, KICK := kDHZ77;
        PR.DHZ77.B, KICK := kDHZ77;
        PR.DHZ79.A, KICK := kDHZ79;
        PR.DHZ79.B, KICK := kDHZ79;
        PR.DHZ81.A, KICK := kDHZ81;
        PR.DHZ81.B, KICK := kDHZ81;
        PR.DHZ83.A, KICK := kDHZ83;
        PR.DHZ83.B, KICK := kDHZ83;
        PR.DHZ85.A, KICK := kDHZ85;
        PR.DHZ85.B, KICK := kDHZ85;
        PR.DHZ87.A, KICK := kDHZ87;
        PR.DHZ87.B, KICK := kDHZ87;
        PR.DHZ89.A, KICK := kDHZ89;
        PR.DHZ89.B, KICK := kDHZ89;
        PR.DHZ91.A, KICK := kDHZ91;
        PR.DHZ91.B, KICK := kDHZ91;
        PR.DHZ93.A, KICK := kDHZ93;
        PR.DHZ93.B, KICK := kDHZ93;
        PR.DHZ95.A, KICK := kDHZ95;
        PR.DHZ95.B, KICK := kDHZ95;
        PR.DHZ97.A, KICK := kDHZ97;
        PR.DHZ97.B, KICK := kDHZ97;
        PR.DHZ99.A, KICK := kDHZ99;
        PR.DHZ99.B, KICK := kDHZ99;