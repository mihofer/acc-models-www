import datetime
import tfs
import glob
import numpy as np
import jinja2
import pandas as pnd
import sys
import httpimport
with httpimport.remote_repo(['webtools'], 'https://gitlab.cern.ch/acc-models/acc-models-ps/-/raw/2021/_scripts/web/'):
    from webtools import webtools
from os.path import relpath
from bokeh.plotting import figure, output_file, output_notebook, show, save, ColumnDataSource
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex

year = datetime.datetime.now().year

directory = '/eos/project/a/acc-models/public/ps/'

branch = '2021'

templateLoader = jinja2.FileSystemLoader( searchpath=directory + "supplementary/_scripts/templates/" )
templateEnv = jinja2.Environment(loader=templateLoader )
ttrans = templateEnv.get_template('transition_crossing.template')
tSC = templateEnv.get_template('space_charge.template')
tnav = templateEnv.get_template('nav.yml.template')

# ---------------------------------------------------------------------
# transition crossing
# ---------------------------------------------------------------------

repo_directory = directory + 'supplementary/transition_crossing/'

scns = webtools.transition_scenarios(repo_directory)

for idx, scn in scns.iterrows():
    rdata = {'scn': scn}
    webtools.renderfile([repo_directory, scn.name], 'index.md', ttrans, rdata)

# ---------------------------------------------------------------------
# space charge example script
# ---------------------------------------------------------------------

repo_directory = directory + branch

# import content of global PS MAD-X files
file_content = pnd.DataFrame(columns = ['filename', 'content'])
file_content['filename'] = sorted(glob.glob(repo_directory + '/ps_*'))
file_content['short_filename'] = file_content['filename'].apply(lambda x: x.split('/')[-1])
labels = ['ps_aper_content', 'ps_mu_content', 'ps_ss_content']

for j, l in enumerate(labels):
    with open(file_content['filename'].iloc[j]) as f:
        file_content['content'].iloc[j] = f.read()
        
k = len(file_content)
    
files = sorted(glob.glob(directory + 'supplementary/space_charge_simulations/*.*'))
files += [repo_directory + '/scenarios/lhc/1_flat_bottom/ps_fb_lhc.str']

for filename in files:
    if ('madx' in filename) or ('ptc' in filename) or ('seq' in filename) or ('str' in filename):

        with open(filename) as f:
            content = f.read()
            
        short_filename = filename.split('/')[-1]
        file_content.loc[k] = [filename, content, short_filename]
        k += 1

rdata = {'date': datetime.datetime.now().strftime("%d/%m/%Y"), 
         'year': str(year),
         'branch': branch,
         'transition_scenarios': scns,
         'ps_mu_seq':  file_content['short_filename'].iloc[1], 
         'ps_mu_content':     file_content['content'].iloc[1],
         'ps_ss_seq':  file_content['short_filename'].iloc[2],
         'ps_ss_content':     file_content['content'].iloc[2],
         'ps_sc':      file_content['short_filename'].iloc[4],
         'ps_sc_content':     file_content['content'].iloc[4],
         'ptc_flat':   file_content['short_filename'].iloc[3],
         'ptc_flat_content':  file_content['content'].iloc[3],
         'seqedit':    file_content['short_filename'].iloc[5],
         'seqedit_content':   file_content['content'].iloc[5],
         'ps_fb_lhc':  file_content['short_filename'].iloc[6],
         'ps_fb_lhc_content': file_content['content'].iloc[6],
         'ps_fb_lhc_rel_path': relpath(file_content['filename'].loc[6], directory + '/supplementary/space_charge_simulations/'),
        }

webtools.renderfile([directory + '/supplementary/space_charge_simulations/'], 'index.md', tSC, rdata)

# ---------------------------------------------------------------------
# Mkdocs navigation file
# ---------------------------------------------------------------------

webtools.renderfile([directory + 'supplementary/'], 'nav.yml', tnav, rdata)
