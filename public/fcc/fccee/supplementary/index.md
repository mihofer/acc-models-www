---
template: overrides/main.html
---

<h1> Future Circular Collider Electron Positron Collider Optics Repository </h1>

The FCC-ee is a high-luminosity, high-energy electron-positron collider, part of the Future Circular Collider (FCC) study.
The double ring collider will be house in a roughly 100 km long tunnel in the Geneva basin and beams will collide in either 2 or 4 interaction points.
The targeted center of mass energies range from 88 GeV to about 365 GeV.

This website gives a brief overview of the iterations of the FCC-ee lattice and optics, together with key parameter. 
The complete dataset is stored in a repository, available on Gitlab, AFS and EOS and accessed in the way described below. 

!!! note "Locations of the repositories on Gitlab, AFS and EOS"
		1) The FCC-ee repository can be found on Gitlab using the following link:

			https://gitlab.cern.ch/acc-models/fcc/fcc-ee-lattice

		2) The repository are also accessible on AFS:

			/afs/cern.ch/eng/acc-models/fcc/fccee

		3) The repository are also accessible on EOS:

			/eos/project/a/acc-models/public/fcc/fccee
