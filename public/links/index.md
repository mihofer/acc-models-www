---
template: overrides/main.html
---

<h1> Useful links </h1>

## Codes

- [BLonD](https://blond.web.cern.ch)
- [MAD-X](http://madx.web.cern.ch/madx/)
- [PyHEADTAIL](https://github.com/PyCOMPLETE/PyHEADTAIL/wiki)
- [PyORBIT](https://github.com/hannes-bartosik/py-orbit)
- [SixTrack](http://sixtrack.web.cern.ch/SixTrack/)

## Committees, Meetings and Working Groups

- [Injectors Performance Panel (IPP)](https://espace.cern.ch/liu-project/Injectors%20Performance%20Panel/Home.aspx)
- [Impedance Working Group](http://impedance.web.cern.ch)
- [LHC Injectors and Experimental Facilities Committee (IEFC)](https://espace.cern.ch/liu-project/Injectors%20Performance%20Panel/Home.aspx)
- [LHC Machine Committee (LMC)](https://espace.cern.ch/lhc-machine-committee/default.aspx)
- [Space Charge Working Group](http://spacecharge.web.cern.ch)

## Databases

- [Controls Configuration Data Editor (CCDE)](https://ccde.cern.ch/dashboard)
- [CERN Drawing Directory](https://edms5.cern.ch/cdd/plsql/c4w.get_in)
- [EDMS](https://edms.cern.ch/)
- [GEODE Database](https://apex-sso.cern.ch/pls/htmldb_accdb/f?p=598:1:12313811622886:::::)
- [Layout Database](https://layout.cern.ch/)
- [Impedance Database](http://impedance.web.cern.ch)
- [Longitudinal Impedance Database](https://longitudinal-impedance.web.cern.ch/)
- [NORMA Database](https://norma-db.web.cern.ch/)
- [TE-EPC Database](https://te-dep-epc-databases.web.cern.ch)

## Documentation

- [NXCALS](http://nxcals-docs.web.cern.ch/current/)

## Maps

- [CERN Geographic Information System (GIS) Portal](https://gis.cern.ch/gisportal/machine.htm)
- [Explore CERN in 3D](https://gisportal.cern.ch/geoportal/apps/webappviewer3d/index.html?id=2234adc8681a4543b8e0115fb7b3cbe3)
- [3D Panoramic images](https://panoramas.web.cern.ch)

## Additional Information

- [OP Webtools](https://op-webtools.web.cern.ch/index.html)
- [OP Vistars](https://op-webtools.web.cern.ch/Vistar/vistars.php)
- [Pick-ups in the CERN PS](http://jeroen.web.cern.ch/jeroen/psring/intro.html)
- [SmarTeam Viewer](https://csviewer.web.cern.ch/document?CDD=false&documentNumber=ST0371486_01&documentVersion=a.02)