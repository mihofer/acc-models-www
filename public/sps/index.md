---
template: overrides/main.html
---

<h1> Super Proton Synchrotron Optics Repository </h1>

This website contains the official optics models for the CERN Super Proton Synchrotron. For each scenario and the various configurations, MAD-X input scripts and
optics plots are available and can be either browsed or downloaded directly. Furthermore, the repository is available on Gitlab, AFS and EOS and can be accessed in the way described below.

!!! note "Locations of the repository on Gitlab, AFS and EOS"
		1) A local copy of the repository on your computer can be obtained by cloning the Gitlab repository using the following syntax:

			git clone https://gitlab.cern.ch/acc-models/acc-models-sps.git

		2) The repository is also accessible from AFS, where only MAD-X scripts and strength files are available:

			/afs/cern.ch/eng/acc-models/sps/

		3) The repository is also accessible from EOS, where all interactive plots can be found in addition to the data available on AFS:

			/eos/project/a/acc-models/public/sps/

The old SPS optics repository is still accessible <a href="http://cern-accelerators-optics.web.cern.ch/SPS/SPS.htm"> here</a>. 