import os
import sys
import pandas as pnd
import numpy as np
import datetime
import jinja2

def renderfile(dirnames, name, template, data):
    basedir=''
    
    for dirname in dirnames:
        basedir = os.path.join(basedir,dirname)

    fullname=os.path.join(basedir,name)
    
    with open(fullname,'w') as indexfile:
        indexfile.write(template.render(**data))
    
    print("Successfully created " + fullname)

repo_directory = '/eos/project/a/acc-models/public/' 

# import content of mkdocs navigation files
filenames = []
accs = ['ad', 'clear', 'elena', 'leir', 'linac3', 'linac4', 'lhc', 'ps', 'psb', 'sps', 'tls', 'ea', 'fccee']

nav_files = pnd.DataFrame(columns = ['filename', 'content'], index = accs)

for acc in accs:
    if 'fcc' in acc:
        nav_files['filename'].loc[acc] = repo_directory + 'fcc/' +  acc + '/nav.yml'
    else:
        nav_files['filename'].loc[acc] = repo_directory + acc + '/nav.yml'

    if os.path.exists(nav_files['filename'].loc[acc]):
        with open(nav_files['filename'].loc[acc]) as f:
            nav_files['content'].loc[acc] = f.read()
    else:
        nav_files['content'].loc[acc] = '        - General: ' + acc + '/index.md'

templateLoader = jinja2.FileSystemLoader( searchpath="/eos/project/a/acc-models/scripts/" )
templateEnv = jinja2.Environment(loader=templateLoader )
tyml = templateEnv.get_template('mkdocs.yml.template')

rdata = {'nav_files': nav_files,
         'date': datetime.datetime.now().strftime("%d/%m/%Y - %H:%M")}

renderfile(['/eos/project/a/acc-models/'], 'mkdocs.yml', tyml, rdata)
