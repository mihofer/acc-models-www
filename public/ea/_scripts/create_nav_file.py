import os
import sys
import pandas as pnd
import numpy as np
import datetime
import jinja2
import sys
sys.path.insert(0, "/eos/project/a/acc-models/public/ea/2021/_scripts/web/")
from webtools import webtools
import glob

repo_directory = 'public/' 

# import content of mkdocs navigation files
folders = sorted(glob.glob('/eos/project/a/acc-models/public/ea/2*/'), reverse=True)# + ['/eos/project/a/acc-models/public/leir/supplementary/']

nav_files = pnd.DataFrame(columns = ['content'], index = folders)

for folder in folders:
    with open(folder + 'nav.yml') as f:
        nav_files['content'].loc[folder] = f.read()

templateLoader = jinja2.FileSystemLoader( searchpath="/eos/project/a/acc-models/public/ea/_scripts/" )
templateEnv = jinja2.Environment(loader=templateLoader )
#tmain = templateEnv.get_template('main.template')
tyml = templateEnv.get_template('nav.yml.template')

rdata = {'nav_files': nav_files}

#webtools.renderfile(['/eos/project/a/acc-models/public/psb'], 'index.md', tmain, rdata)
webtools.renderfile(['/eos/project/a/acc-models/public/ea'], 'nav.yml', tyml, rdata)