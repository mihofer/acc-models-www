#!/bin/bash
echo
echo 'Executing Python scripts...'
# moving survey folders to supplementary directories
mv /eos/project-a/acc-models/public/ps/2021/survey/* /eos/project-a/acc-models/public/ps/supplementary/survey
rm -r /eos/project-a/acc-models/public/ps/2021/survey

mv /eos/project-a/acc-models/public/psb/2021/survey/* /eos/project-a/acc-models/public/psb/supplementary/survey
rm -r /eos/project-a/acc-models/public/psb/2021/survey

mv /eos/project-a/acc-models/public/leir/2021/survey/* /eos/project-a/acc-models/public/leir/supplementary/survey
rm -r /eos/project-a/acc-models/public/leir/2021/survey
# creating web content for PS, PSB and LEIR supplementary sites
/usr/bin/python3 /eos/project-a/acc-models/public/ps/supplementary/_scripts/web_creation_supplementary.py
/usr/bin/python3 /eos/project-a/acc-models/public/psb/supplementary/_scripts/web_creation_supplementary.py
/usr/bin/python3 /eos/project-a/acc-models/public/leir/supplementary/_scripts/web_creation_supplementary.py
# combining mkdocs navigation structures for PS, PSB and LEIR optics and supplementary information
/usr/bin/python3 /eos/project/a/acc-models/public/ps/_scripts/create_nav_file.py
/usr/bin/python3 /eos/project/a/acc-models/public/psb/_scripts/create_nav_file.py
/usr/bin/python3 /eos/project/a/acc-models/public/leir/_scripts/create_nav_file.py
/usr/bin/python3 /eos/project/a/acc-models/public/sps/_scripts/create_nav_file.py
/usr/bin/python3 /eos/project/a/acc-models/public/ea/_scripts/create_nav_file.py
/usr/bin/python3 /eos/project/a/acc-models/public/fcc/fccee/_scripts/create_nav_file.py

# creating the overall mkdocs configuration file
/usr/bin/python3 /eos/project-a/acc-models/scripts/create_mkdocs_config_file.py

# actually building the website into the directory 'site'
echo 
echo 'Executing mkdocs to build the website...'
cd /eos/project-a/acc-models/
/afs/cern.ch/user/a/accmodels/.local/bin/mkdocs build

echo
echo 'Copying files to destination...'
# copying all html files from site to public and removing the site folder
cp -r /eos/project-a/acc-models/site/assets/ /eos/project-a/acc-models/site/404.html /eos/project-a/acc-models/site/sitemap.xml /eos/project-a/acc-models/site/sitemap.xml.gz /eos/project-a/acc-models/site/search/ /eos/project-a/acc-models/public/
cd /eos/project-a/acc-models/site
find . -name "index.html" | cpio -pm ../public/
cd ..
rm -r site
echo 'Done.'
