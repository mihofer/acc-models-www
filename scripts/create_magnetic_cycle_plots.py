import os
import sys
import glob
import httpimport
with httpimport.remote_repo(['webtools'], 'https://gitlab.cern.ch/acc-models/acc-models-ps/-/raw/2021/_scripts/web/'):
    from webtools import webtools

filenames = []

repo_directory = '/eos/project/a/acc-models/public/' 

accs = ['leir', 'psb']
accs = ['leir']

for acc in accs:
    files = glob.glob(repo_directory + acc + '/experimental_data/magnetic_field/2018/*.pkl')

    for f in files:
        webtools.plot_magnetic_cycle(f, f[:-3] + 'html', 'file')