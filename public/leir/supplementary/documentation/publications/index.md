<h1> Documentation and publications </h1>

Important information about LEIR are collected on this webpage.


<h2>Drawings</h2>

- LEIR layout drawings of the ring and the SSs [LEILJ___000X](https://edms5.cern.ch/cdd/plsql/c4w_guided.guided_drawing?cookie=2467566&p_equip_id=LJ___){target=_blank}

- LEIR layout drawings of the main dipoles [LEILMBHN000X](https://edms5.cern.ch/cdd/plsql/c4w_guided.guided_drawing?cookie=2467566&p_equip_id=LMBHN){target=_blank}

- Overview drawings including information on assemblies, elements, vacuum chambers, drawing references etc.
	- [Drawing references](IMPLANTATION_LEIR_1.pdf) 
	- [Element dimensions](IMPLANTATION_LEIR_2.pdf) 
	- [Full Excel spreadsheet](Implantation_LEIR.xlsx)

<h2>Links</h2>

- [LEIR Electron Cooler Website](https://electron-cooling.web.cern.ch)

- [Ions for LHC Project Website](https://project-i-lhc.web.cern.ch/CommissioningLEIR.htm)

<h2>Publications</h2>

- P.Lefevre, D.Mohl, [A low energy accumulation ring of ions for LHC (a feasibility study)](http://cds.cern.ch/record/279362/files/ps-93-062.pdf)

- G. Plass, [Design study of a facility for experiments with low energy antiprotons (LEAR)](http://cds.cern.ch/record/124681/)

- M. Chanel, [LEIR : The low energy ion ring at CERN](http://cdsweb.cern.ch/record/557588/)

- J. Pasternak et al., [LEIR lattice](http://cdsweb.cern.ch/record/972662/)

- L. Soby et al., [LEIR beam instrumentation](http://cdsweb.cern.ch/record/972662/)

- D. Korchagin et al., [LEIR closed orbit measurement system](http://cdsweb.cern.ch/record/972662/)

- M. Paoluzzi et al., [The LEIR RF system](http://cdsweb.cern.ch/record/851054/)

- E. Mahner, [The Vacuum System of the Low Energy Ion Ring at CERN : Requirements, Design, and Challenges](http://cdsweb.cern.ch/record/902810)

- K. Schindl et al., [The LHC Lead Injector Chain](http://cdsweb.cern.ch/record/792709/)

- A. Fowler, [The Injection Bumper System for LEIR](http://cdsweb.cern.ch/record/738817/)

- J. Borburgh et al., [The Septa for LEIR Extraction and PS Injection](http://cdsweb.cern.ch/record/971814/)

- J. Borburgh et al., [Design and Construction of the LEIR Injection Septa](http://cdsweb.cern.ch/record/841091/)

- J. Borburgh et al., [Design and Construction of the LEIR Extraction Septum](http://cdsweb.cern.ch/record/931388/)

- C. Carli et al., [LEIR Commissioning](http://cdsweb.cern.ch/record/972341/)