---
template: overrides/main.html
---

<h1> CERN Optics Repository </h1>

This website contains the official optics models for the CERN accelerator complex. To browse through the different optics scenarios, select any accelerator from the top menu.

!!! note "Locations of the repositories on Gitlab, AFS and EOS"
		1) The Gitlab group acc-models, which hosts the different optics repositories, can be accessed via the link in the upper right corner of this website or at the following address:

			https://gitlab.cern.ch/acc-models/

		2) The repositories are also accessible on AFS:

			/afs/cern.ch/eng/acc-models/

		3) The repositories are also accessible on EOS, where the website is hosted and all interactive optics plots can be found:

			/eos/project/a/acc-models/public/


<a href="https://cds.cern.ch/record/2684277/files/CCC-v2019-final-white.png?subformat=icon-1440&version=1" target=_blank>
  <img title="CERN Accelerator Complex" src="https://cds.cern.ch/record/2684277/files/CCC-v2019-final-white.png?subformat=icon-640&version=1">
</a>

The old optics repository is still available at this <a href="http://cern-accelerators-optics.web.cern.ch/cern-accelerators-optics/" target=_blank>location</a>.
