---
template: overrides/main.html
---

<h1> Low Energy Ion Ring Optics Repository </h1>

This website contains the official optics models for the CERN Low Energy Ion Ring. For each scenario listed below 
(and the various configurations belonging to each scenario), MAD-X input scripts and
optics plots are available and can be either browsed or downloaded directly. Furthermore, the repository is available on Gitlab, AFS and EOS and can be accessed in the way described below.

!!! note "Locations of the repository on Gitlab, AFS and EOS"
		1) A local copy of the repository on your computer can be obtained by cloning the Gitlab repository using the following syntax:

			git clone https://gitlab.cern.ch/acc-models/acc-models-leir.git

		2) The repository is also accessible from AFS, where only MAD-X scripts and strength files are available:

			/afs/cern.ch/eng/acc-models/leir/

		3) The repository is also accessible from EOS, where all interactive plots can be found in addition to the data available on AFS:

			/eos/project/a/acc-models/public/leir/

The old LEIR optics repository is still accessible <a href="http://project-leir-optics.web.cern.ch/project-LEIR-optics/2012/"> here</a>. 