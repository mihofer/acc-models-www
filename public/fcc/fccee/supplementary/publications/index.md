---
template: overrides/main.html
---

<h1> Publications </h1>


On this website, a small number of references to publications relevant to the design of the FCC-ee lattices is provided.
In addition, links to meeting of the different design subgroups are found below.

To add links and references, please change the files in [this gitlab directory](https://gitlab.cern.ch/acc-models/acc-models-www/-/tree/master/public/fcc/fccee/supplementary).

## Publications

??? abstract "FCC-ee: The Lepton Collider : Future Circular Collider Conceptual Design Report Volume 2, `M. Benedikt et al.`, [Eur. Phys. J. Spec. Top. 228 (2019) 261-623](https://cds.cern.ch/record/2651299?ln=en){target=_blank}"
    ```
    @techreport{Benedikt:2651299,
      author        = "Benedikt, Michael and Blondel, Alain and Brunner, Olivier
                       and Capeans Garrido, Mar and Cerutti, Francesco and
                       Gutleber, Johannes and Janot, Patrick and Jimenez, Jose
                       Miguel and Mertens, Volker and Milanese, Attilio and Oide,
                       Katsunobu and Osborne, John Andrew and Otto, Thomas and
                       Papaphilippou, Yannis and Poole, John and Tavian, Laurent
                       Jean and Zimmermann, Frank",
      title         = "{FCC-ee: The Lepton Collider: Future Circular Collider
                       Conceptual Design Report Volume 2. Future Circular
                       Collider}",
      institution   = "CERN",
      address       = "Geneva",
      reportNumber  = "CERN-ACC-2018-0057, 2",
      month         = "Dec",
      year          = "2018",
      url           = "https://cds.cern.ch/record/2651299",
      doi           = "10.1140/epjst/e2019-900045-4",
	}
    ```

??? abstract "Design of beam optics for the future circular collider e+e− collider rings, `K. Oide et al.`, [Phys. Rev. Accel. Beams 19, 111005](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.19.111005){target=_blank}"
    ```
	@article{PhysRevAccelBeams.19.111005,
	title = {Design of beam optics for the future circular collider ${e}^{+}{e}^{\ensuremath{-}}$ collider rings},
	author = {Oide, K. and Aiba, M. and Aumon, S. and Benedikt, M. and Blondel, A. and Bogomyagkov, A. and Boscolo, M. and Burkhardt, H. and Cai, Y. and Doblhammer, A. and Haerer, B. and Holzer, B. and Jowett, J. M. and Koop, I. and Koratzinos, M. and Levichev, E. and Medina, L. and Ohmi, K. and Papaphilippou, Y. and Piminov, P. and Shatilov, D. and Sinyatkin, S. and Sullivan, M. and Wenninger, J. and Wienands, U. and Zhou, D. and Zimmermann, F.},
	journal = {Phys. Rev. Accel. Beams},
	volume = {19},
	issue = {11},
	pages = {111005},
	numpages = {11},
	year = {2016},
	month = {Nov},
	publisher = {American Physical Society},
	doi = {10.1103/PhysRevAccelBeams.19.111005},
	url = {https://link.aps.org/doi/10.1103/PhysRevAccelBeams.19.111005}
	}
    ```

??? abstract "Challenges for the Interaction Region Design of the Future Circular Collider FCC-ee, `M. Boscolo et al.`, [Proc. 12th Int. Particle Accelerator Conf. (IPAC’21)](http://accelconf.web.cern.ch/ipac2021/papers/wepab029.pdf){target=_blank}"
    ```
	@InProceedings{Boscolo:IPAC21-WEPAB029,
		author = {M. Boscolo and others},
		title = {Challenges for the Interaction Region Design of the Future Circular Collider FCC-ee},
		booktitle = {Proc. 12th Int. Particle Accelerator Conf. (IPAC'21)},
		pages = {2668--2671},
		paper = {WEPAB029},
		venue = {Campinas, Brazil},
		publisher = {JACoW Publishing},
		month = {May},
		year = {2021},
		doi = {doi:10.18429/JACoW-IPAC2021-WEPAB029},
		note = {https://doi.org/10.18429/JACoW-IPAC2021-WEPAB029},
		url = {https://jacow.org/ipac2021/papers/WEPAB029.pdf},
		language = {english}
	}
    ```

## Meeting categories
	- [FCC conferences and workshops](https://indico.cern.ch/category/5225/)
	- [FCC-ee optics design meetings](https://indico.cern.ch/category/6528/)
	- [FCC-ee technology and R&D meetings](https://indico.cern.ch/category/13637/)
	- [FCC-ee MDI meetings](https://indico.cern.ch/category/5665/)
	- [FCC-ee EPOL meetings](https://indico.cern.ch/category/8678/)