<h1> Publications </h1>

Important publications related to the PS are centralized on this webpage.


<h2>The Proton Synchrotron</h2>

- G. Plass, [The CERN proton synchrotron: 50 years of reliable operation and continued development](https://doi.org/10.1140/epjh/e2011-20059-0){target=_blank}

- S. Gilardoni et al., [Fifty years of the CERN Proton Synchrotron: Volume 1](https://cds.cern.ch/record/1359959){target=_blank}

- E. Regenstreif, [The CERN Proton Synchrotron, pt.1](https://cds.cern.ch/record/214352){target=_blank}

- E. Regenstreif, [The CERN Proton Synchrotron, pt.2](https://cds.cern.ch/record/102347){target=_blank}


<h2>The Proton Synchrotron magnets</h2>

- H. Umst&auml;tter, [The effective length of the PS magnets at various field levels](https://edms.cern.ch/document/2026379/0.1){target=_blank}

- K. Reich, [The CERN Proton Synchrotron magnet](https://cds.cern.ch/record/110059){target=_blank}

- E. Regenstreif, [The CERN Proton Synchrotron, pt.3](https://cds.cern.ch/record/278715){target=_blank}

- C. Iselin, [Measurements on the Prototype Magnet Unit](http://cds.cern.ch/record/1108738){target=_blank}


<h2>The LHC Injectors Upgrade</h2>

- M. Meddahi et al., [LHC Injectors Upgrade Project: Towards New Territory Beam Parameters](http://jacow.org/ipac2019/papers/thxplm1.pdf){target=_blank}

- H. Damerau et al., [LHC Injectors Upgrade, Technical Design Report, Vol. I: Protons](https://cds.cern.ch/record/1976692){target=_blank}


<h2>The Multi-Turn Extraction</h2>

- S. Machida et al., [Numerical investigation of space charge effects on the positions of beamlets for transversely split beams](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.20.121001){target=_blank}

- A. Huschauer et al., [Transverse beam splitting made operational: Key features of the multiturn extraction at the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.20.061001){target=_blank}

- S. Abernethy et al., [Operational performance of the CERN injector complex with transversely split beams](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.20.014001){target=_blank}

- J. Borburgh et al., [First implementation of transversely split proton beams in the CERN Proton Synchrotron for the fixed-target physics programme](https://doi.org/10.1209/0295-5075/113/34001){target=_blank}

- S. Gilardoni et al., [Experimental evidence of adiabatic splitting of charged particle beams using stable islands of transverse phase space](https://link.aps.org/doi/10.1103/PhysRevSTAB.9.104001){target=_blank}

- M. Giovannozzi et al., [The CERN PS multi-turn extraction based on beam splittting in stable islands of transverse phase space: Design Report](https://cds.cern.ch/record/987493){target=_blank}

- R. Cappi et al., [Novel Method for Multiturn Extraction: Trapping Charged Particles in Islands of Phase Space](https://link.aps.org/doi/10.1103/PhysRevLett.88.104801){target=_blank}


<h2>Space charge studies</h2>

- G. Franchetti et al., [Space charge effects on the third order coupled resonance](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.20.081006){target=_blank}

- G. Franchetti et al., [Space charge and octupole driven resonance trapping observed at the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevSTAB.6.124201){target=_blank}


<h2>Transverse instabilities</h2>

- M. Migliorati et al., [Instability studies at the CERN Proton Synchrotron during transition crossing](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.21.120101){target=_blank}

- S. Persichelli et al., [Transverse beam coupling impedance of the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.19.041001){target=_blank}

- E. Mahner et al., [Electron cloud detection and characterization in the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevSTAB.11.094401){target=_blank}

- M. Giovannozzi et al., [Electron cloud buildup and instability: Numerical simulations for the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevSTAB.6.010101){target=_blank}

- R. Cappi et al., [Electron cloud buildup and related instability in the CERN Proton Synchrotron](https://link.aps.org/doi/10.1103/PhysRevSTAB.5.094401){target=_blank}


<h2>Theses</h2>

- R. Wasef, [8th Order Super-Structure Resonance driven by Space Charge](https://cds.cern.ch/record/2670261){target=_blank}

- A. Huschauer, [Beam Dynamics Studies for High-Intensity Beams in the CERN Proton Synchrotron](https://cds.cern.ch/record/2194332){target=_blank}

- S. Persichelli, [The beam coupling impedance model of CERN Proton Synchrotron](https://cds.cern.ch/record/2027523){target=_blank}

- N. Biancacci, [Improved techniques of impedance calculation and localization in particle accelerators](https://cds.cern.ch/record/1704527){target=_blank}

- M. Juchno, [Magnetic Model of the CERN Proton Synchrotron](https://cds.cern.ch/record/2129023){target=_blank}

- S. Aumon, [High Intensity Beam Issues in the CERN Proton Synchrotron](https://cds.cern.ch/record/1517412){target=_blank}

- A. Huschauer, [Working point and resonance studies at the CERN Proton Synchrotron](https://cds.cern.ch/record/1501943){target=_blank}

- M. Juchno, [Electromagnetic FEM analysis of the CERN Proton Synchrotron main magnetic unit](https://cds.cern.ch/record/1314838){target=_blank}






